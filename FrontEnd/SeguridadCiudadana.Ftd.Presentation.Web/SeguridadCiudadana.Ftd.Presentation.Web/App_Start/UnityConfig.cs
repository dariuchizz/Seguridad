using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SeguridadCiudadana.Ftd.Presentation.Web.Models;
using SeguridadCiudadana.Ftd.Presentation.Web.Controllers;
using SeguridadCiudadana.Fwk.Infrastructure.Data.Core;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Application.Core.Services;

namespace SeguridadCiudadana.Ftd.Presentation.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<UserManager<ApplicationUser>>();
            //container.RegisterType<DbContext, ApplicationDbContext>();
            container.RegisterType<ApplicationUserManager>();
            container.RegisterType<AccountController>(new InjectionConstructor());

            //-> Unit of Work 
            container.RegisterType<IQueryableUnitOfWork, SeguridadCiudadanaEntities>(new PerResolveLifetimeManager(), new InjectionConstructor());

            //-> Providers


            //-> Repositories
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new TransientLifetimeManager());

            //-> Application services
            container.RegisterType<ISecurityAppService, SecurityAppService>(new TransientLifetimeManager());
            container.RegisterType<IConfigurationAppService, ConfigurationAppService>(new TransientLifetimeManager());
            container.RegisterType<IAlertAppService, AlertAppService>(new TransientLifetimeManager());


        }
    }
}
