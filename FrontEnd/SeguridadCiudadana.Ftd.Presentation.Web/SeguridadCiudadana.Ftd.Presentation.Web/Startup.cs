﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SeguridadCiudadana.Ftd.Presentation.Web.Startup))]
namespace SeguridadCiudadana.Ftd.Presentation.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
