﻿using SeguridadCiudadana.Ftd.Presentation.Web.Resources;
using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend.Controllers
{
    public class DispositivoController : Controller
    {
        private readonly IAlertAppService _alertAppService;
        private readonly ISecurityAppService _securityAppService;


        public DispositivoController(IAlertAppService alertAppService, ISecurityAppService securityAppService)
        {
            _alertAppService = alertAppService;
            _securityAppService = securityAppService;

        }
        // GET: Backend/Dispositivo
        public ActionResult Index()
        {
            return View();
        }
        // GET: Dispositivo/Create
        public ActionResult Create()
        {
            LoadViewsBags();
            var viewModel = new Dispositivo()
            {
                UserId = Session["UserIdSelected"].ToString()

            };
            return View(viewModel);
        }

        // POST: Dispositivo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Dispositivo dispositivo,FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Random rnd = new Random();

                    var codigoSeguridad = rnd.Next(0, 99999).ToString("00000");
                    var usuario = this._securityAppService.GetUserById(Session["UserIdSelected"].ToString());

                    var device = new Dispositivo()
                    {

                        CodigoSeguridad = codigoSeguridad,
                        UserId = Session["UserIdSelected"].ToString(),
                        Marca = dispositivo.Marca,
                        Modelo = dispositivo.Modelo,
                        Compania = dispositivo.Compania,
                        NumeroTelefonico = Session["PrefijoTelefonico"]   +  dispositivo.NumeroTelefonico,
                        TipoDispositivo = int.Parse(collection[2]),
                        Estado = (int)EstadoDispositivo.Activo
                };
                    _alertAppService.CreateDevice(device);
                    SendEmail(usuario.Email, Messages.AddDeviceEmailSubject, string.Format(Messages.AddDeviceEmailBody, dispositivo.NumeroTelefonico, dispositivo.Marca, dispositivo.Modelo, codigoSeguridad));

                    return  RedirectToAction("Dispositivos","AspNetUsers", new { id = Session["UserIdSelected"].ToString() });

                }
                catch (Exception ex)
                { }
            }

            LoadViewsBags();
            return View(dispositivo);
        }

        public ActionResult Edit(int id)
        {
            LoadViewsBags();
            var device = this._alertAppService.GetDeviceById(id);
            device.NumeroTelefonico = device.NumeroTelefonico.Substring(6);

            return View(device);
        }

        // POST: Dispositivo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Dispositivo dispositivo, int id, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    
                    var device = this._alertAppService.GetDeviceById(id);

                    device.Marca = dispositivo.Marca;
                    device.Modelo = dispositivo.Modelo;
                    device.Compania = dispositivo.Compania;
                    device.NumeroTelefonico = Session["PrefijoTelefonico"] + dispositivo.NumeroTelefonico;
                    device.TipoDispositivo = int.Parse(collection[2]);


                    _alertAppService.EditDevice(device);
                    //SendEmail(device.AspNetUsers.Email, Messages.AddDeviceEmailSubject, string.Format(Messages.AddDeviceEmailBody, dispositivo.NumeroTelefonico, dispositivo.Marca, dispositivo.Modelo, codigoSeguridad));

                    return RedirectToAction("Dispositivos", "AspNetUsers", new { id = Session["UserIdSelected"].ToString() });

                }
                catch (Exception ex)
                { }
            }

            LoadViewsBags();
            return View(dispositivo);
        }


        public ActionResult Suspender(int id)
        {
            LoadViewsBags();
            var device = this._alertAppService.GetDeviceById(id);
            

            return View(device);
        }

        // POST: Dispositivo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Suspender(Dispositivo dispositivo, int id, FormCollection collection)
        {
            try
            {

                var device = this._alertAppService.GetDeviceById(id);

                device.Estado = (int)EstadoDispositivo.Inactivo;


                _alertAppService.EditDevice(device);
                //SendEmail(device.AspNetUsers.Email, Messages.AddDeviceEmailSubject, string.Format(Messages.AddDeviceEmailBody, dispositivo.NumeroTelefonico, dispositivo.Marca, dispositivo.Modelo, codigoSeguridad));

                return RedirectToAction("Dispositivos", "AspNetUsers", new { id = Session["UserIdSelected"].ToString() });

            }
            catch (Exception ex)
            { }
           

            LoadViewsBags();
            return View(dispositivo);
        }

        public ActionResult Activar(int id)
        {
            LoadViewsBags();
            var device = this._alertAppService.GetDeviceById(id);
            
            return View(device);
        }

        // POST: Dispositivo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Activar(Dispositivo dispositivo, int id, FormCollection collection)
        {
           
            try
            {

                var device = this._alertAppService.GetDeviceById(id);

                device.Estado = (int)EstadoDispositivo.Activo;


                _alertAppService.EditDevice(device);
                //SendEmail(device.AspNetUsers.Email, Messages.AddDeviceEmailSubject, string.Format(Messages.AddDeviceEmailBody, dispositivo.NumeroTelefonico, dispositivo.Marca, dispositivo.Modelo, codigoSeguridad));

                return RedirectToAction("Dispositivos", "AspNetUsers", new { id = Session["UserIdSelected"].ToString() });

            }
            catch (Exception ex)
            { }
            

            LoadViewsBags();
            return View(dispositivo);
        }

        public ActionResult MarcaAutoCompletar(string id)
        {
            var marcas = Enum.GetValues(typeof(Marca)).Cast<Marca>();
            return Json(marcas
                .Where(x=> x.ToString().ToUpper().Contains(id.ToUpper()) )
                .Take(10)
                .Select(c => new
                {
                    nombre = c.ToString()
                }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompaniaAutoCompletar(string id)
        {
            var companias = Enum.GetValues(typeof(Compania)).Cast<Compania>();
            return Json(companias
                .Where(x => x.ToString().ToUpper().Contains(id.ToUpper()))
                .Take(10)
                .Select(c => new
                {
                    nombre = c.ToString()
                }), JsonRequestBehavior.AllowGet);
        }

        private void LoadViewsBags()
        {

            ViewBag.DeviceTypes = new SelectList(from TipoDispositivo e in Enum.GetValues(typeof(TipoDispositivo))
                                                   select new { ID = (int)e, Name = e.ToString() }, "ID", "Name");

            Session["PrefijoTelefonico"] = ConfigurationManager.AppSettings["telephonePrefix"];

        }

        private void SendEmail(string userEmail, string subject, string body)
        {

            string siteEmail = ConfigurationManager.AppSettings["siteEmail"];
            string siteEmailPassword = ConfigurationManager.AppSettings["siteEmailPassword"];
            string siteName = ConfigurationManager.AppSettings["siteName"];
            string smtpHost = ConfigurationManager.AppSettings["smtpHost"];

            MailAddress from = new MailAddress(siteEmail, siteName);
            MailMessage mailmsj = new MailMessage();
            mailmsj.From = from;
            MailAddress to = new MailAddress(userEmail);
            mailmsj.Bcc.Add(to);
            mailmsj.Subject = subject;
            mailmsj.Body = body;
            mailmsj.IsBodyHtml = true;

            try
            {
                SmtpClient client = new SmtpClient();
                client.Credentials = new System.Net.NetworkCredential(siteEmail, siteEmailPassword);
                client.Host = smtpHost;
                client.Port = 587;
                client.EnableSsl = true;
                client.Send(mailmsj);
            }
            catch (Exception ex)
            {
                LoggerFactory.Instance.LogError("Error Envió Mail {0}: {1}", subject, ex.Message);
            }
            /*
            var to = new string[] { userEmail };
            var cc = new string[] { };
            var bcc = new string[] { };
            Task.Factory.StartNew(() => MailingFactory.Instance.Current.SendEmail(to, cc, bcc, subject, body, siteEmail)).ContinueWith((task) => LoggerFactory.Instance.LogError("Error al enviar mail. Detalle: {0}", task.Exception.Message), TaskContinuationOptions.OnlyOnFaulted);
            */
        }

    }
}