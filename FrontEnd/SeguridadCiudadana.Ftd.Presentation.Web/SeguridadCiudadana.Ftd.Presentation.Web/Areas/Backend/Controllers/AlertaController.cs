﻿using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using SeguridadCiudadana.Fwk.Domain.Core.States.Alertas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend.Controllers
{
    public class AlertaController : Controller
    {
        private readonly IAlertAppService _alertAppService;
        private readonly ISecurityAppService _securityAppService;


        public AlertaController(IAlertAppService alertAppService, ISecurityAppService securityAppService)
        {
            _alertAppService = alertAppService;
            _securityAppService = securityAppService;

        }
        // GET: Backend/Alerta
        public ActionResult Index()
        {
            ViewBag.Operations = Enum.GetNames(typeof(Operacion)).ToList();
            var alertas = _alertAppService.GetTopFiveAlerts();
            return View(alertas);
        }

        // GET: Backend/Alerta/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Backend/Alerta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Backend/Alerta/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Backend/Alerta/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Backend/Alerta/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Backend/Alerta/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Backend/Alerta/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //GetAllLocation - for fetch all the location from database and show in the view
        //Shows all the locations in default map here.
        public JsonResult GetAllLocation()
        {
            var v = this._alertAppService.GetTopFiveAlerts().Select(x => new { Id = x.AlertaId, Title = x.Dispositivo.DescripcionLargaDispositivo, Address = x.Direccion, ImagePath = "/Content/" + x.TipoAlerta + ".jpg", Latitud = x.Latitud, Longitud = x.Longitud }).ToList();
            return new JsonResult { Data = v, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }
        //This method gets the markers info from database.
        public JsonResult GetMarkerData(int locationID)
        {

            var v = this._alertAppService.GetAlertsById(locationID).Select(x => new { Id = x.AlertaId, Title = x.Dispositivo.DescripcionLargaDispositivo, Address =  x.Direccion, ImagePath = "/Content/" + x.TipoAlerta+".jpg", Latitud = x.Latitud, Longitud = x.Longitud }).ToList();

            return new JsonResult { Data = v, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        
        public ActionResult AddAlertHistory(int? alertaId, int? estado, string observacion)
        {
                var a = this._alertAppService.GetAlertById(alertaId.Value);

                var alertaHistoria = new AlertaHistoria()
                {
                    Alerta = a,
                    FechaHora = DateTime.Now,
                    Estado = estado,
                    Observacion = observacion,
                    UserId = this._securityAppService.GetUserByName(User.Identity.Name).Id,
                    CurrentState = AlertaStateFactory.CreateState((Estado)estado.Value)
                };
            
                this._alertAppService.CreateAlertHistory(alertaHistoria);


                //return new JsonResult { Data = alertaHistoria, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                return PartialView("History", _alertAppService.GetAlertById(alertaHistoria.AlertaId).AlertasHistoria);


        }


    }
}
