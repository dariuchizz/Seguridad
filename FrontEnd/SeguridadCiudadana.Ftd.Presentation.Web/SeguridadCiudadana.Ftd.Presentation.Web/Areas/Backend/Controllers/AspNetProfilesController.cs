﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeguridadCiudadana.Fwk.Infrastructure.Data.Core;
using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Domain.Core;

namespace SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend.Controllers
{
    //[SeguridadCiudadanaAuthorize(Roles = "Root")]
    public class AspNetProfilesController : Controller
    {
        
        
        private readonly ISecurityAppService _securityAppService;

        private readonly SeguridadCiudadanaEntities _db;

        public AspNetProfilesController(ISecurityAppService securityAppService)
        {
            _securityAppService = securityAppService;
            _db = new SeguridadCiudadanaEntities();

        }

        // GET: AspNetProfiles
        public async Task<ActionResult> Index(bool? esTransaccionExitosa)
        {
            if (esTransaccionExitosa.HasValue && esTransaccionExitosa.Value)
                ViewBag.esTransaccionExitosa = true;
            //return View(_db.AspNetProfiles.Where(x => x.Activo));
            return View(_db.AspNetProfiles);
        }

        // GET: AspNetProfiles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetProfiles aspNetProfiles = await _db.AspNetProfiles.FindAsync(id);
            if (aspNetProfiles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetProfiles);
        }

        // GET: AspNetProfiles/Create
        public ActionResult Create()
        {
            var viewModel = new AspNetProfiles()
            {
                AspNetRoles = _db.AspNetRoles.Where(x => x.Activo).OrderBy(x => x.Name).ToList()
            };
            return View(viewModel);
        }

        // POST: AspNetProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AspNetProfiles aspNetProfiles)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var perfil = new AspNetProfiles
                    {
                        Id  = Guid.NewGuid().ToString(),
                        Name = aspNetProfiles.Name,
                        Activo = true,
                        AspNetRoles = new List<AspNetRoles>()
                    };

                    foreach (var item in aspNetProfiles.AspNetRoles.Where(p => p.IsSelected))
                    {
                        var rol = await _db.AspNetRoles.FindAsync(item.Id);
                        rol.AspNetProfiles.Add(perfil);
                    }

                    _db.AspNetProfiles.Add(perfil);
                    await _db.SaveChangesAsync();

                    return RedirectToAction("Index", new { esTransaccionExitosa = true });

                }
                catch (Exception ex)
                { }
            }

            return View(aspNetProfiles);
        }

        // GET: AspNetProfiles/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var perfil = await _db.AspNetProfiles.FindAsync(id);
            if (perfil == null)
            {
                return HttpNotFound();
            }
            var rolesIds = perfil.AspNetRoles.Select(r => r.Id);


            //Cargar todos los roles (para poder ser mostrados)
            perfil.AspNetRoles = _db.AspNetRoles.Where(x => x.Activo).ToList();

            //Poner como chequeado los que estan relacionados al perfil
            foreach (var r in perfil.AspNetRoles)
                r.IsSelected = rolesIds.Contains(r.Id);

            return View(perfil);

        }

        // POST: AspNetProfiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AspNetProfiles aspNetProfiles, string id)
        {
            if (ModelState.IsValid)
            {
                var perfil = await _db.AspNetProfiles.FindAsync(id);

                if (perfil != null)
                {
                    //GUARDAR LAS PROPIEDADES ESCALARES
                    //_db.Entry(perfil).CurrentValues.SetValues(aspNetProfiles);//fer

                    perfil.Name = aspNetProfiles.Name;
                                        
                    perfil.Activo = true;

                    foreach (var rol in aspNetProfiles.AspNetRoles)
                    {
                        #region ASOCIAR O ELIMINAR EL ROL
                        //ASOCIAR NUEVO ROL
                        if (rol.IsSelected)
                        {
                            if (perfil.AspNetRoles.All(r => r.Id != rol.Id))
                            {
                                var r = new AspNetRoles()
                                {
                                    Id = rol.Id 
                                };
                                _db.AspNetRoles.Attach(r);
                                perfil.AspNetRoles.Add(r);
                            }
                        }
                        //ELIMINAR ASOCIACION
                        else
                        {
                            var removedRol = perfil.AspNetRoles.SingleOrDefault(c => c.Id == rol.Id);
                            if (removedRol != null)
                                perfil.AspNetRoles.Remove(removedRol);
                        }
                        #endregion
                    }
                    _db.Entry(perfil).State = EntityState.Modified;
                    _db.SaveChanges();

                    #region ACTUALIZAR USUARIOS QUE TIENEN ESE PERFIL
                    var userList = _db.AspNetUsers.Where(x => x.AspNetProfiles.Any(p => p.Id == perfil.Id));

                    foreach (var usuario in userList.ToList())
                    {
                        foreach (var r in perfil.AspNetRoles.ToList())
                        {
                            if (perfil.AspNetRoles.Contains(r))
                            {
                                usuario.AspNetRoles.Add(r);
                            }
                            else
                            {
                                usuario.AspNetRoles.Remove(r);
                            }
                        }

                        foreach (var r in usuario.AspNetRoles.ToList())
                        {
                            if (!perfil.AspNetRoles.Contains(r))
                            {
                                usuario.AspNetRoles.Remove(r);
                            }
                        }
                        _db.Entry(usuario).State = EntityState.Modified;
                    }
                    _db.SaveChanges();
                    #endregion

                    return RedirectToAction("Index", new { esTransaccionExitosa = true });
                }
            }
            return View(aspNetProfiles);
        }

        // GET: AspNetProfiles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetProfiles aspNetProfiles = await _db.AspNetProfiles.FindAsync(id);
            if (aspNetProfiles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetProfiles);
        }

        // POST: AspNetProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            AspNetProfiles aspNetProfiles = await _db.AspNetProfiles.FindAsync(id);
            aspNetProfiles.Activo = false;
            _db.Entry(aspNetProfiles).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index", new { esTransaccionExitosa = true });
        }

        [HttpPost, ActionName("Suspender")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Suspender(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var perfil = await _db.AspNetProfiles.FindAsync(id);

            if (perfil == null)
            {
                return HttpNotFound();
            }

            perfil.Activo = false;
            _db.Entry(perfil).State = EntityState.Modified;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index", new { esTransaccionExitosa = true });
        }

        [HttpPost, ActionName("Activar")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Activar(string id)
        {
            var perfil = await _db.AspNetProfiles.FindAsync(id);

            if (perfil == null)
            {
                return HttpNotFound();
            }

            perfil.Activo = true;
            _db.Entry(perfil).State = EntityState.Modified;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index", new { esTransaccionExitosa = true });

        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
            //    managerDispose();
            //}
            base.Dispose(disposing);
        }
    }
}
