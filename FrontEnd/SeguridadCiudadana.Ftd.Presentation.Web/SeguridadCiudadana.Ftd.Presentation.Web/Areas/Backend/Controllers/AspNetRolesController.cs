﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Infrastructure.Data.Core;
using SeguridadCiudadana.Fwk.Domain.Core;

namespace SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend.Controllers
{
    //[CIMTUCAuthorize(Roles = "Root")]
    public class AspNetRolesController : Controller
    {

        private readonly ISecurityAppService _securityAppService;

        private readonly SeguridadCiudadanaEntities _db;

        public AspNetRolesController(ISecurityAppService securityAppService)
        {
            _securityAppService = securityAppService;
            _db = new SeguridadCiudadanaEntities();

        }

        // GET: AspNetRoles
        public async Task<ActionResult> Index(bool? esTransaccionExitosa)
        {
            if (esTransaccionExitosa.HasValue && esTransaccionExitosa.Value)
                ViewBag.esTransaccionExitosa = true;
            
            return View(_securityAppService.GetAllRoles());
        }

        // GET: AspNetRoles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetRoles aspNetRoles = await _aspNetRolesAppService.GetByIdAsync(id);
            AspNetRoles aspNetRoles = await _db.AspNetRoles.FindAsync(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // GET: AspNetRoles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AspNetRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AspNetRoles aspNetRoles)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    aspNetRoles.Id = Guid.NewGuid().ToString();
                    aspNetRoles.Activo = true;

                    //await _aspNetRolesAppService.CreateAsync(aspNetRoles);//fer
                    _securityAppService.CreateRol(aspNetRoles);

                    return RedirectToAction("Index", new { esTransaccionExitosa = true });
                }
                catch (Exception ex)
                { }
            }
            
            return View(aspNetRoles);
        }

        // GET: AspNetRoles/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetRoles aspNetRoles = await _aspNetRolesAppService.GetByIdAsync(id);//fer
            AspNetRoles aspNetRoles = await _db.AspNetRoles.FindAsync(id);

            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // POST: AspNetRoles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AspNetRoles aspNetRoles)
        {
            if (ModelState.IsValid)
            {
                aspNetRoles.Activo = true;
                //await _aspNetRolesAppService.EditAsync(aspNetRoles);//fer
                _securityAppService.EditRol(aspNetRoles);

                return RedirectToAction("Index", new { esTransaccionExitosa = true });
            }
            return View(aspNetRoles);
        }

        // GET: AspNetRoles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetRoles aspNetRoles = await _aspNetRolesAppService.GetByIdAsync(id); // fer
            AspNetRoles aspNetRoles = await _db.AspNetRoles.FindAsync(id);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetRoles);
        }

        // POST: AspNetRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            //AspNetRoles aspNetRoles = await _aspNetRolesAppService.GetByIdAsync(id); //fer
            AspNetRoles aspNetRoles = await _db.AspNetRoles.FindAsync(id);
            //await _aspNetRolesAppService.DeleteAsync(aspNetRoles); //fer
            _securityAppService.DeleteRol(aspNetRoles);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Suspender")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Suspender(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetRoles aspNetRole = await _aspNetRolesAppService.GetByIdAsync(id);
            AspNetRoles aspNetRole = await _db.AspNetRoles.FindAsync(id);

            if (aspNetRole == null)
            {
                return HttpNotFound();
            }

            //Validar si hay algun usuario activo en este rol
            if (aspNetRole.AspNetUsers.Where(u => u.AspNetUserClaims.Any(c => c.ClaimType == "active" && c.ClaimValue == "true")).Count() > 0)
            {
                ModelState.AddModelError("", "El rol no se puede deshabilitar porque hay usuarios activos asociados a él.");
            }
            else
            {
                aspNetRole.Activo = false;
                //await _aspNetRolesAppService.EditAsync(aspNetRole); //fer
                _securityAppService.EditRol(aspNetRole);
            }
            return View("index", _securityAppService.GetAllRoles());
        }

        [HttpPost, ActionName("Activar")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Activar(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetRoles aspNetRole = await _aspNetRolesAppService.GetByIdAsync(id);
            AspNetRoles aspNetRole = await _db.AspNetRoles.FindAsync(id);

            if (aspNetRole == null)
            {
                return HttpNotFound();
            }

            aspNetRole.Activo = true;
            //await _aspNetRolesAppService.EditAsync(aspNetRole);
            _securityAppService.EditRol(aspNetRole);

            return View("index", _securityAppService.GetAllRoles());
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
            //    managerDispose();
            //}
            base.Dispose(disposing);
        }
    }
}
