﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Web.Security;
using System.Data.Entity.Validation;
using System.Diagnostics;
using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Infrastructure.Data.Core;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Ftd.Presentation.Web.Utils;
using SeguridadCiudadana.Ftd.Presentation.Web.Models;
using SeguridadCiudadana.Ftd.Presentation.Web.Resources;
using System.Configuration;
using SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.Core.Mailing;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Logging;
using System.Net.Mail;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;

namespace SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend.Controllers
{
    //[SeguridadCiudadanaAuthorize(Roles = "Root")]
    public class AspNetUsersController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly ISecurityAppService _securityAppService;
        private readonly IConfigurationAppService _configurationAppService;
        private readonly IAlertAppService _alertAppService;

        private readonly SeguridadCiudadanaEntities _db;

        public AspNetUsersController(ISecurityAppService securityAppService, IConfigurationAppService configurationAppService, IAlertAppService alertAppService)
        {
            _securityAppService = securityAppService;
            _configurationAppService = configurationAppService;
            _alertAppService = alertAppService;
            _db = new SeguridadCiudadanaEntities();

        }

        // GET: AspNetUsers
        //[CIMTUCAuthorize(Roles = "Usuarios-Listado, Root")]
        public async Task<ActionResult> Index(bool? esTransaccionExitosa)
        {
            if (esTransaccionExitosa.HasValue && esTransaccionExitosa.Value)
                ViewBag.esTransaccionExitosa = true;

            return View(_securityAppService.GetAllPersons());
            //return View(_db.AspNetUsers);
        }

        // GET: AspNetUsers/Details/5
        //[SeguridadCiudadanaAuthorize(Roles = "Usuarios-Listado, Root")]
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetUsers aspNetUsers = await _managerAspNetUsers.GetByIdAsync(id);//fer
            AspNetUsers aspNetUsers = await _db.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // GET: AspNetUsers/Create
        //[SeguridadCiudadanaAuthorize(Roles = "Usuarios-Alta, Root")]
        public ActionResult Create()
        {
            //Mostrar imagen por defecto
            Persona personasViewModel = new Persona();
            personasViewModel.AspNetUsers = new AspNetUsers();

            Bitmap img = new Bitmap(Server.MapPath("~/Content/user.png"));
            personasViewModel.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            personasViewModel.AspNetUsers.AspNetProfiles = _db.AspNetProfiles.Where(x => x.Activo).OrderBy(x => x.Name).ToList();

            LoadViewsBags("Formulario de Registro");

            if (User.Identity.IsAuthenticated)
            {
                personasViewModel.AspNetUsers.AgreeTerms = true;
                ViewBag.Titulo = "Crear Usuario";
            }
            return View(personasViewModel);
        }

        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[SeguridadCiudadanaAuthorize(Roles = "Usuario-Crear, Root")]
        public async Task<ActionResult> Create(Persona model, FormCollection collection)
        {
            
            #region VALIDAR MAIL
            
            if (_db.personas.Any(x => x.Email.ToLower().Trim() == model.Email.ToLower().Trim()))
            {
                ModelState.AddModelError("Mail", "Este mail se encuentra asociado a otro usuario.");
            }

            #endregion

            #region PROCESAR IMAGEN
            Bitmap img;
            img = string.IsNullOrEmpty(model.PathImagen) ? new Bitmap(Server.MapPath("~/Content/user.png")) : new Bitmap(Server.MapPath(model.PathImagen));

            model.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);

            if (model.Imagen != null) //Guardar en carpeta temporal
            {
                model.PathImagen = GuardarImagen(model.Imagen, "Temp", "0");
                img = new Bitmap(Server.MapPath((string)model.PathImagen));
                model.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            if (ModelState.IsValid)
            {
                #region CREAR PERSONA
                var persona = new Persona()
                {
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    Email = model.Email,
                    Documento = model.Documento,
                    TipoDocumento = int.Parse(collection[7]),
                    Genero = model.Genero,
                    FechaRegistracion = DateTime.Now,
                    FechaNacimiento = model.FechaNacimiento,
                    TelefonoCelular  = model.TelefonoCelular,
                    TelefonoOpcional = model.TelefonoOpcional,
                    Calle = model.Calle,
                    CodigoPostal = model.CodigoPostal,
                    PaisId = model.PaisId,
                    ProvinciaId = model.ProvinciaId,
                    LocalidadId = model.LocalidadId,
                };
                #endregion

                var user = new ApplicationUser { UserName = model.AspNetUsers.UserName, Email = model.Email};
                
                var result = await UserManager.CreateAsync(user, model.AspNetUsers.Password);
                if (result.Succeeded)
                {
                    var newUser = await _db.AspNetUsers.FindAsync(user.Id);
                    await UserManager.AddClaimAsync(newUser.Id , new Claim("active", "false"));

                    #region GUARDAR PERFILES Y USUARIOS
                    foreach (var item in model.AspNetUsers.AspNetProfiles.Where(p => p.IsSelected))
                    {
                        var perfil = await _db.AspNetProfiles.FindAsync(item.Id);
                        perfil.AspNetUsers.Add(newUser);
                        foreach (var rol in perfil.AspNetRoles)
                        {
                            var r = await _db.AspNetRoles.FindAsync(rol.Id);
                            r.AspNetUsers.Add(newUser);
                        }
                    }
                    #endregion

                    persona.AspNetUsers = newUser;
                    persona.UserId
                         = newUser.Id;

                    _db.personas.Add(persona);
                    
                    try
                    {
                        await _db.SaveChangesAsync();
                        await SendConfirmationMail(model.Email, user.Id);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage);
                            }
                        }

                        throw;  // You can also choose to handle the exception here...
                    }
                   

                    #region MOVER ARCHIVOS
                    if (!string.IsNullOrEmpty(model.PathImagen) && model.PathImagen.StartsWith("\\Files\\Temp\\"))
                    {
                        var destPath = $"\\Files\\Usuarios\\{model.UserId}";
                        var fileName = Path.GetFileName(model.PathImagen);

                        string sourceFile = Server.MapPath(model.PathImagen);
                        string destinationFile = Server.MapPath(Path.Combine(destPath, fileName));

                        // Ensure that the target does not exist.
                        if (System.IO.File.Exists(destinationFile))
                            System.IO.File.Delete(destinationFile);

                        model.PathImagen = Path.Combine(destPath, fileName);

                        if (!Directory.Exists(destPath))
                            Directory.CreateDirectory(Server.MapPath(destPath));

                        // Move the file.
                        System.IO.File.Copy(sourceFile, destinationFile);
                    }
                    #endregion

                    _db.SaveChanges();
                    return RedirectToAction("Index", new { esTransaccionExitosa = true });
                }
                AddErrors(result);
            }
            LoadViewsBags("Formulario de Registro");
            
            return View(model);
        }

       

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: AspNetUsers/Edit/5
        //[CIMTUCAuthorize(Roles = "Usuarios-Edicion, Root")]
        public async Task<ActionResult> Edit(int id)
        {
            ViewBag.esEdicion = true;
           

            Persona persona = await _db.personas.FindAsync(id);
            if (persona == null)
            {
                return HttpNotFound();
            }

            #region PROCESAR IMAGEN
            string imageBase64 = string.Empty;

            if (!string.IsNullOrEmpty(persona.PathImagen))
            {
                try
                {
                    Bitmap img = new Bitmap(Server.MapPath(persona.PathImagen));
                    imageBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
                    persona.ImagenBase64 = imageBase64;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Error", e.Message);
                }
            }
            else
            {
                Bitmap img = new Bitmap(Server.MapPath("~/Content/user.png"));
                persona.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            #region MOSTRAR PERFILES SELECCIONADOS PARA ESE USUARIO
            if (persona.AspNetUsers != null)
            { 
                var perfilesIds = persona.AspNetUsers.AspNetProfiles.Select(r => r.Id);
                //Cargar todos los perfiles al usuario (para poder ser mostrados)
                persona.AspNetUsers.AspNetProfiles = _db.AspNetProfiles.Where(x => x.Activo).ToList();
            
            //Poner como chequeado los que estan relacionados al perfil
            foreach (var p in persona.AspNetUsers.AspNetProfiles)
                p.IsSelected = perfilesIds.Contains(p.Id);

            }
            #endregion

            LoadViewsBags("Editar Usuario");

            return View(persona);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[SeguridadCiudadanaAuthorize(Roles = "Usuario-Editar, Root")]
        public async Task<ActionResult> Edit(Persona model, FormCollection collection)
        {
                                

            #region VALIDAR MAIL
            //Validar que no se repita el mail del usuario
            if (_db.personas.Any(x => x.Email.ToLower().Trim() == model.Email.ToLower().Trim() && x.PersonaId  != model.PersonaId))
            {
                ModelState.AddModelError("Mail", "Este mail se encuentra asociado a otro usuario.");
            }
            #endregion

            #region PROCESAR IMAGEN
            Bitmap img;
            img = string.IsNullOrEmpty(model.PathImagen) ? new Bitmap(Server.MapPath("~/Content/user.png")) : new Bitmap(Server.MapPath(model.PathImagen));

            model.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            ModelState.Remove("PathImagen");

            if (model.Imagen != null) //Guardar en carpeta tempporal
            {
                model.PathImagen = GuardarImagen(model.Imagen, "Temp", model.PersonaId.ToString());
                img = new Bitmap(Server.MapPath((string)model.PathImagen));
                model.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            #region DATOS GENERALES
            var personaOrig = await _db.personas.FindAsync(model.PersonaId );
            
            //personaOrig.Activo = true;

            personaOrig.Nombre = model.Nombre;
            personaOrig.Apellido = model.Apellido;
            personaOrig.Email = model.Email;
            personaOrig.Documento = model.Documento;
            personaOrig.TipoDocumento = int.Parse(collection[7]);
            personaOrig.Genero = model.Genero;
            //personaOrig.FechaRegistracion = DateTime.Now;
            personaOrig.FechaNacimiento = model.FechaNacimiento;
            personaOrig.TelefonoCelular = model.TelefonoCelular;
            personaOrig.TelefonoOpcional = model.TelefonoOpcional;
            personaOrig.Calle = model.Calle;
            personaOrig.CodigoPostal = model.CodigoPostal;
            personaOrig.PaisId = model.PaisId;
            personaOrig.ProvinciaId = model.ProvinciaId;
            personaOrig.LocalidadId = model.LocalidadId;
            #endregion

            if (ModelState.IsValid)
            {
                #region DATOS DE USUARIO
                AspNetUsers userDB = personaOrig.AspNetUsers;

                if (userDB == null)
                {
                    
                    var user = new ApplicationUser { UserName = model.AspNetUsers.UserName, Email = model.Email};
                
                    var result = await UserManager.CreateAsync(user, model.AspNetUsers.Password);

                    if (result.Succeeded)
                    {
                        userDB = await _db.AspNetUsers.FindAsync(user.Id);
                        await UserManager.AddClaimAsync(userDB.Id, new Claim("active", "false"));
                    }
                     

                }
                
                var userModel = model.AspNetUsers;
                if (!string.IsNullOrEmpty(model.AspNetUsers.Password) && personaOrig.AspNetUsers != null)
                {
                    var resetToken = await UserManager.GeneratePasswordResetTokenAsync(userDB.Id );
                    var result = await UserManager.ResetPasswordAsync(userModel.Id, resetToken, userModel.Password);
                    if (!result.Succeeded)
                    {
                        AddErrors(result);
                    }
                }
                #endregion

                #region DATOS DE PERFILES Y ROLES

                foreach (var perfil in userModel.AspNetProfiles.ToList())
                {
                    var perfilDB = await _db.AspNetProfiles.FindAsync(perfil.Id);

                    //ASOCIAR NUEVO PERFIL Y LOS ROLES DE ESE PERFIL
                    if (perfil.IsSelected)
                    {
                        if (userDB.AspNetProfiles.All(p => p.Id != perfil.Id))
                        {
                            userDB.AspNetProfiles.Add(perfilDB);
                            //ASOCIAR LOS ROLES DEL PERFIL AL USUARIO
                            foreach (var rol in perfilDB.AspNetRoles)
                            {
                                var r = await _db.AspNetRoles.FindAsync(rol.Id);
                                r.AspNetUsers.Add(userDB);
                            }
                        }
                    }
                    //ELIMINAR ASOCIACION
                    else
                    {
                        //Eliminar Perfil
                        userDB.AspNetProfiles.Remove(perfilDB);
                        //Eliminar Roles (solo si no hay un perfil activo que lo contiene)
                        foreach (var rol in perfilDB.AspNetRoles)
                        {
                            var rolDB = await _db.AspNetRoles.FindAsync(rol.Id);

                            if (rol.AspNetProfiles.Any(x => x.Id != perfilDB.Id && x.AspNetUsers.Contains(userDB)))
                            {
                            }
                            else
                            {
                                userDB.AspNetRoles.Remove(rolDB);
                            }
                        }
                    }
                }
                #endregion

                #region MOVER ARCHIVOS
                if (!string.IsNullOrEmpty(model.PathImagen) && model.PathImagen.StartsWith("\\Files\\Temp\\"))
                {
                    var destPath = $"\\Files\\Usuarios\\{model.UserId}";
                    var fileName = Path.GetFileName(model.PathImagen);

                    string sourceFile = Server.MapPath(model.PathImagen);
                    string destinationFile = Server.MapPath(Path.Combine(destPath, fileName));

                    // Ensure that the target does not exist.
                    if (System.IO.File.Exists(destinationFile))
                        System.IO.File.Delete(destinationFile);

                    personaOrig.PathImagen = Path.Combine(destPath, fileName);

                    if (!Directory.Exists(destPath))
                        Directory.CreateDirectory(Server.MapPath(destPath));

                    // Move the file.
                    System.IO.File.Copy(sourceFile, destinationFile);
                }
                #endregion

                _db.Entry(personaOrig).State = EntityState.Modified;

                personaOrig.AspNetUsers = userDB;
                try
                {
                    await _db.SaveChangesAsync();
                    await this.SendConfirmationMail(personaOrig.Email, userDB.Id);
                }
                catch (Exception)
                {
                }
                return RedirectToAction("Index", new { esTransaccionExitosa = true });
            }

            ViewBag.esEdicion = true;
            ViewBag.crearUsuario = Session["CreateUser"];
            LoadViewsBags("Editar Usuario");

            return View(model);
        }

        public async Task<ActionResult> MiPerfil(bool? esTransaccionExitosa)
        {
            if (esTransaccionExitosa.HasValue && esTransaccionExitosa.Value)
                ViewBag.esTransaccionExitosa = true;

            ViewBag.esEdicion = true;

            Persona persona = _db.personas.FirstOrDefault(x => x.AspNetUsers.UserName == User.Identity.Name);
            if (persona == null || persona.AspNetUsers == null)
            {
                return HttpNotFound();
            }

            #region PROCESAR IMAGEN
            string imageBase64 = string.Empty;

            if (!string.IsNullOrEmpty(persona.PathImagen))
            {
                try
                {
                    Bitmap img = new Bitmap(Server.MapPath(persona.PathImagen));
                    imageBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
                    persona.ImagenBase64 = imageBase64;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Error", e.Message);
                }
            }
            else
            {
                Bitmap img = new Bitmap(Server.MapPath("~/Content/user.png"));
                persona.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            return View(persona);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> MiPerfil(Persona model)
        {
            if (string.IsNullOrEmpty(model.AspNetUsers.UserName))
                ModelState.AddModelError("AspNetUsers.UserName", "Este campo es obligatorio");
            if (string.IsNullOrEmpty(model.Apellido))
                ModelState.AddModelError("Apellido", "Este campo es obligatorio");
            if (string.IsNullOrEmpty(model.Email))
                ModelState.AddModelError("Mail", "Este campo es obligatorio");

            if (!string.IsNullOrEmpty(model.AspNetUsers.Password))
            {
                if (!string.IsNullOrEmpty(model.AspNetUsers.ConfirmPassword) &&
                    model.AspNetUsers.ConfirmPassword.Length < 6)
                    ModelState.AddModelError("AspNetUsers.ConfirmPassword", "Debe ingresar al menos 6 caracteres");
                if (!string.IsNullOrEmpty(model.AspNetUsers.Password) && model.AspNetUsers.Password.Length < 6)
                    ModelState.AddModelError("AspNetUsers.Password", "Debe ingresar al menos 6 caracteres");
            }

            #region VALIDAR MAIL
            //Validar que no se repita el mail del usuario
            if (_db.personas.Any(x => x.Email.ToLower().Trim() == model.Email.ToLower().Trim() && x.PersonaId != model.PersonaId ))
            {
                ModelState.AddModelError("Mail", "Este mail se encuentra asociado a otro usuario.");
            }
            #endregion

            ViewBag.esEdicion = true;

            #region PROCESAR IMAGEN
            Bitmap img;
            img = string.IsNullOrEmpty(model.PathImagen) ? new Bitmap(Server.MapPath("~/Content/user.png")) : new Bitmap(Server.MapPath(model.PathImagen));

            model.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            ModelState.Remove("PathImagen");

            if (model.Imagen != null) //Guardar en carpeta tempporal
            {
                model.PathImagen = GuardarImagen(model.Imagen, "Temp", model.PersonaId.ToString());
                img = new Bitmap(Server.MapPath((string)model.PathImagen));
                model.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            #region DATOS GENERALES
            var personaOrig = await _db.personas.FindAsync(model.PersonaId);
            personaOrig.Nombre = model.Nombre;
            personaOrig.Apellido = model.Apellido;
            personaOrig.Email = model.Email;
            //personaOrig.Activo = true;
            #endregion

            if (ModelState.IsValid)
            {
                #region DATOS DE USUARIO
                var userDB = personaOrig.AspNetUsers;
                var userModel = model.AspNetUsers;
                if (!string.IsNullOrEmpty(model.AspNetUsers.Password))
                {
                    var resetToken = await UserManager.GeneratePasswordResetTokenAsync(userDB.Id );
                    var result = await UserManager.ResetPasswordAsync(userModel.Id , resetToken, userModel.Password);
                    if (!result.Succeeded)
                    {
                        AddErrors(result);
                    }
                }
                #endregion

                #region MOVER ARCHIVOS
                if (!string.IsNullOrEmpty(model.PathImagen) && model.PathImagen.StartsWith("\\Files\\Temp\\"))
                {
                    var destPath = $"\\Files\\Usuarios\\{model.UserId}";
                    var fileName = Path.GetFileName(model.PathImagen);

                    string sourceFile = Server.MapPath(model.PathImagen);
                    string destinationFile = Server.MapPath(Path.Combine(destPath, fileName));

                    // Ensure that the target does not exist.
                    if (System.IO.File.Exists(destinationFile))
                        System.IO.File.Delete(destinationFile);

                    personaOrig.PathImagen = Path.Combine(destPath, fileName);

                    if (!Directory.Exists(destPath))
                        Directory.CreateDirectory(Server.MapPath(destPath));

                    // Move the file.
                    System.IO.File.Copy(sourceFile, destinationFile);
                }
                #endregion

                _db.Entry(personaOrig).State = EntityState.Modified;
                try
                {
                    await _db.SaveChangesAsync();
                }
                catch (Exception)
                {
                }
                return RedirectToAction("MiPerfil", new {username = userModel.UserName, esTransaccionExitosa = true });
            }

            var p = await _db.personas.FindAsync(model.PersonaId);
            model.AspNetUsers.AspNetProfiles = p.AspNetUsers.AspNetProfiles;

            return View(model);
        }

        // GET: AspNetUsers/Delete/5
        //[CIMTUCAuthorize(Roles = "Usuarios-Borrado, Root")]
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AspNetUsers aspNetUsers = await _managerAspNetUsers.GetByIdAsync(id);//fer
            AspNetUsers aspNetUsers = await _db.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        public async Task<ActionResult> DatosUsuario(string username, bool? returnPartial)
        {
            ViewBag.esEdicion = true;

            Persona persona = _db.personas.FirstOrDefault(x => x.AspNetUsers.UserName == username);
            if (persona == null || persona.AspNetUsers == null) 
            {
                return HttpNotFound();
            }

            #region PROCESAR IMAGEN
            string imageBase64 = string.Empty;

            if (!string.IsNullOrEmpty(persona.PathImagen))
            {
                try
                {
                    Bitmap img = new Bitmap(Server.MapPath(persona.PathImagen));
                    imageBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
                    persona.ImagenBase64 = imageBase64;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Error", e.Message);
                }
            }
            else
            {
                Bitmap img = new Bitmap(Server.MapPath("~/Content/user.png"));
                persona.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            if (returnPartial.HasValue && returnPartial.Value)
            {
                ViewBag.returnPartial = true;
                return PartialView(persona);
            }
            else
            {
                return View(persona);
            }
        }


        public async Task<ActionResult> DatosPersona(string idPersona, bool? returnPartial)
        {
            ViewBag.esEdicion = true;

            Persona persona = await _db.personas.FindAsync(int.Parse(idPersona));
            if (persona == null)
            {
                return HttpNotFound();
            }

            #region PROCESAR IMAGEN
            string imageBase64 = string.Empty;

            if (!string.IsNullOrEmpty(persona.PathImagen))
            {
                try
                {
                    Bitmap img = new Bitmap(Server.MapPath(persona.PathImagen));
                    imageBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
                    persona.ImagenBase64 = imageBase64;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Error", e.Message);
                }
            }
            else
            {
                Bitmap img = new Bitmap(Server.MapPath("~/Content/user.png"));
                persona.ImagenBase64 = ImageUtils.ImageToBase64(img, ImageFormat.Png);
            }
            #endregion

            if (returnPartial.HasValue && returnPartial.Value)
            {
                ViewBag.returnPartial = true;
                return PartialView(persona);
            }
            else
            {
                return View(persona);
            }
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[SeguridadCiudadanaAuthorize(Roles = "Usuario-Suspender, Root")]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            //AspNetUsers aspNetUsers = await _managerAspNetUsers.GetByIdAsync(id);//fer
            AspNetUsers aspNetUsers = await _db.AspNetUsers.FindAsync(id);
            //await _managerAspNetUsers.DeleteAsync(aspNetUsers); //fer
            this._securityAppService.DeleteUser(aspNetUsers);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
            //    managerDispose();
            //}
            base.Dispose(disposing);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Suspender")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Suspender(string id)
        {

            var claim = _securityAppService.GetUserClaimByUserId(id, "active");
            if (claim != null)
                claim.ClaimValue = "false";

            try
            {

                this._securityAppService.EditUserClaim(claim);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                if (e.InnerException != null && e.InnerException.Message != null)
                {
                    ModelState.AddModelError("", e.InnerException.Message);
                }
            }
            return View("index", _db.personas.Where(x => x.UserId != null));
        }

        
        [HttpPost, ActionName("Activar")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Activar(string id)
        {
            
            var claim = _securityAppService.GetUserClaimByUserId(id, "active");
            if (claim != null)
                claim.ClaimValue = "true";
            
            try
            {
            
                 this._securityAppService.EditUserClaim(claim);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                if (e.InnerException != null && e.InnerException.Message != null)
                {
                    ModelState.AddModelError("", e.InnerException.Message);
                }
            }
            return View("index", _db.personas.Where(x => x.UserId != null));
        }

        
        public ActionResult Dispositivos(string id)
        {
            Session["UserNameSelected"] = _securityAppService.GetUserById(id).UserName;
            Session["UserIdSelected"] = id;
            return View("Dispositivos", _alertAppService.GetDevicesByUserId(id) );
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetStates(int paisId)
        {
            var provincias = this._configurationAppService.FindStatesByCountryId(paisId);
            var jsonResult = Json(provincias.Select(s => new { ProvinciaId = s.ProvinciaId, Descripcion = s.Descripcion }), JsonRequestBehavior.AllowGet);
            return jsonResult;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetCities(int provinciaId)
        {
            var localidades = this._configurationAppService.FindCitiesByStateId(provinciaId);
            var jsonResult = Json(localidades.Select(s => new { LocalidadId = s.LocalidadId, Descripcion = s.Descripcion }), JsonRequestBehavior.AllowGet);
            return jsonResult;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetPostalCode(int localidadId)
        {
            
            var jsonResult = Json(this._configurationAppService.GetLocalidadById(localidadId).CodigoPostal, JsonRequestBehavior.AllowGet);
            return jsonResult;
        }

        #region Private Methods

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private string GuardarImagen(HttpPostedFileBase imagen, string folderName, string actor)
        {
            if (imagen != null && imagen.ContentLength > 0)
            {
                try
                {
                    var fileName = Path.GetFileName(imagen.FileName);
                    var pathToSave = $"\\Files\\{folderName}\\{actor}";
                    string path = Server.MapPath(pathToSave);
                    Directory.CreateDirectory(path);
                    var fullFileName = Path.Combine(path, fileName);
                    imagen.SaveAs(fullFileName);
                    return Path.Combine(pathToSave, fileName);
                }
                catch (Exception)
                {

                }
            }
            return null;
        }

        private async Task SendConfirmationMail(string email, string userId)
        {
            //ENVIAR MAIL DE CONFIRMACION

            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { area = "", userId = userId, code = code }, protocol: Request.Url.Scheme);

            this.SendEmail(email, "Confirme su cuenta", "Por favor confirme su cuenta haciendo click <a href=\"" + callbackUrl + "\">aquí</a>");
        }

        private void SendEmail(string userEmail, string subject, string body)
        {
            
            string siteEmail = ConfigurationManager.AppSettings["siteEmail"];
            string siteEmailPassword = ConfigurationManager.AppSettings["siteEmailPassword"];
            string siteName = ConfigurationManager.AppSettings["siteName"];
            string smtpHost = ConfigurationManager.AppSettings["smtpHost"];

            MailAddress from = new MailAddress(siteEmail, siteName);
            MailMessage mailmsj = new MailMessage();
            mailmsj.From = from;
            MailAddress to = new MailAddress(userEmail);
            mailmsj.Bcc.Add(to);
            mailmsj.Subject = subject;
            mailmsj.Body = body;
            mailmsj.IsBodyHtml = true;

            try
            {
                SmtpClient client = new SmtpClient();
                client.Credentials = new System.Net.NetworkCredential(siteEmail, siteEmailPassword);
                client.Host = smtpHost;
                client.Port = 587;
                client.EnableSsl = true;
                client.Send(mailmsj);
            }
            catch (Exception ex)
            {
                LoggerFactory.Instance.LogError("Error Envió Mail {0}: {1}", subject, ex.Message);
            }
            /*
            var to = new string[] { userEmail };
            var cc = new string[] { };
            var bcc = new string[] { };
            Task.Factory.StartNew(() => MailingFactory.Instance.Current.SendEmail(to, cc, bcc, subject, body, siteEmail)).ContinueWith((task) => LoggerFactory.Instance.LogError("Error al enviar mail. Detalle: {0}", task.Exception.Message), TaskContinuationOptions.OnlyOnFaulted);
            */
        }

        private void LoadViewsBags(string title)
        {
            var paises = this._configurationAppService.FindCountries().ToList();
            paises.Add(new Pais() { PaisId = 0, Descripcion = "Seleccione..." });
            ViewBag.Paises = new SelectList(paises.OrderBy(x => x.PaisId), "PaisId", "Descripcion");

            ViewBag.DocumentTypes = new SelectList(from TipoDocumento e in Enum.GetValues(typeof(TipoDocumento))
                                                   select new { ID = (int)e, Name = e.ToString() }, "ID", "Name");

            ViewBag.Genders = new SelectList(from Genero e in Enum.GetValues(typeof(Genero))
                                             select new { ID = e, Name = e.ToString() }, "ID", "Name");

            ViewBag.Titulo = title;
          

        }



        #endregion
    }
}
