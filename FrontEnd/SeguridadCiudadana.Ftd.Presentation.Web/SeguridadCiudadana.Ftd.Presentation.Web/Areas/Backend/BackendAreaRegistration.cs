﻿using System.Web.Mvc;

namespace SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend
{
    public class BackendAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Backend";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Backend_default",
                "Backend/{controller}/{action}/{id}",
                new { controller = "Alerta",  action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "SeguridadCiudadana.Ftd.Presentation.Web.Areas.Backend.Controllers" }
            );
        }
    }
}