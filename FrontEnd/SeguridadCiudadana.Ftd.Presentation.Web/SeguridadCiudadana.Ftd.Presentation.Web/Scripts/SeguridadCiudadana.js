﻿$(document).ready(function () {

    $('#Marca').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Dispositivo/MarcaAutoCompletar", type: "POST", dataType: "json",
                data: { id: request.term },
                success: function (data) {

                    $("#loading").css("display", "none");
                    response($.map(data, function (item) {
                        return {
                            label: item.nombre,
                            value: item.nombre,
                            id: item.id
                        };
                    }));
                },
                beforeSend: function (data) {
                   
                    $("#loading").css("display", "block");
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            
        }

    });
    $('#Compania').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Dispositivo/CompaniaAutoCompletar", type: "POST", dataType: "json",
                data: { id: request.term },
                success: function (data) {

                    $("#loading").css("display", "none");
                    response($.map(data, function (item) {
                        return {
                            label: item.nombre,
                            value: item.nombre,
                            id: item.id
                        };
                    }));
                },
                beforeSend: function (data) {

                    $("#loading").css("display", "block");
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {

        }

    });


});

function submitSuspender(id) {
            var input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("name", "id");
            input.setAttribute("value", id);
            document.getElementById("suspenderForm").appendChild(input);
            document.getElementById('suspenderForm').submit();
        }
function submitActivar(id) {
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", "id");
    input.setAttribute("value", id);
    document.getElementById("activarForm").appendChild(input);
    document.getElementById('activarForm').submit();
}
function submitDispositivos(id) {
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", "id");
    input.setAttribute("value", id);
    document.getElementById("dispositivosForm").appendChild(input);
    document.getElementById('dispositivosForm').submit();
}

function AddAlertHistory(alertaId, estado) {

    
    var observacion = prompt("Observación: ");

    $.ajax({
        url: "/Backend/Alerta/AddAlertHistory", type: "POST", cache: false,
        data: { alertaId: alertaId, estado: estado, observacion: observacion },
        success: function (data) {

            //$("#loading").css("display", "none");
            $("#history_" + alertaId).html(data);
            
           
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Se ha producido un error")
        }
    });
        
}

