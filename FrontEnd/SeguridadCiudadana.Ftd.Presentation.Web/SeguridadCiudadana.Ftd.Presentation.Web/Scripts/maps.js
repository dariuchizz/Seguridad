﻿var app = angular.module('myapp', ['uiGmapgoogle-maps']); //dependency we should add to angular application
app.controller('mapsController', function ($scope, $http) {
    //this is default coordinates for the map when it loads for first time
    $scope.map = { center: { latitude: -27.3494343, longitude: -65.6139757 }, zoom: 16 }
    $scope.markers = [];
    $scope.locations = [];
    
    //to get all the locations from the server
    $http.get('/Backend/Alerta/GetAllLocation').then(function (data) {
      
        $scope.locations = data.data;
    }, function () {
        alert('Error');
    });
    //service that gets makers info from server
    $scope.ShowLocation = function (locationID) {
        $http.get('/Backend/Alerta/GetMarkerData', {
            params: {
                locationID: locationID
            }
        }).then(function (data) {
            $scope.markers = [];
            $scope.markers.push({
                id: data.data[0].Id,
                coords: { latitude: data.data[0].Latitud, longitude: data.data[0].Longitud },
                title: data.data[0].Title,     //title of the makers
                address: data.data[0].Address,     //Address of the makers
                image: data.data[0].ImagePath     //image --optional
            });
            //set map focus to center

            $scope.map.center.latitude = data.data[0].Latitud;
            $scope.map.center.longitude = data.data[0].Longitud;
        }, function () {
            alert('Error'); //shows error if connection lost or error occurs
        });
    }
    //Show or Hide marker on map using options passes here
    $scope.windowOptions = {
        show: true
    };
});