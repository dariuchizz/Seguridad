﻿using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.IoC;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Ftd.Presentation.Configuration
{
    public static class SiteConfigurationManager
    {
        #region Properties
        public static Guid SiteCode
        {
            get
            {
                Guid id;
                return Guid.TryParse(ConfigurationManager.AppSettings["siteCode"], out id) ? id : Guid.NewGuid();
            }
        }

        public static string ServiceUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["serviceUrl"].ToString();
            }
        }

        public static int ServicePort
        {
            get {
                int port;
                return int.TryParse(ConfigurationManager.AppSettings["servicePort"], out port) ? port : 0;
            }
        }

        public static int SiteId
        {
            get
            {
                int site;
                return int.TryParse(ConfigurationManager.AppSettings["siteId"], out site) ? site : 0;
            }
        }

        #endregion

        #region Constructor
        static SiteConfigurationManager()
        {
          
        }
        #endregion

    }
}
