﻿using SeguridadCiudadana.Services.Application.MainContext.DTO.AlertModule;
using SeguridadCiudadana.Services.Application.MainContext.DTO.DeviceModule;
using SeguridadCiudadana.Services.Application.MainContext.DTO.PersonModule;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace SeguridadCiudadana.Services.DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAlertaModuleService
    {

        [OperationContract]
        string CheckDevice(string phoneNumber, string securityCode);

        [OperationContract]
        DispositivoDTO RegisterDevice(string phoneNumber, string securityCode);
                
        [OperationContract]
        bool SendAlert(string latitud, string longitud, string direccion, int tipo_alerta, int dispositivoId);

        [OperationContract]
        PersonaDTO CreatePerson(string apellido, string nombre, int documento, string fecha_nacimiento, string telefono_celular, string telefono_opcional, string email);

        [OperationContract]
        PersonaDTO UpdatePerson(int documento);
        
        
        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
