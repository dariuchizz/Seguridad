﻿using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.DistributedServices.Core.ErrorHandlers;
using SeguridadCiudadana.Services.Application.MainContext.DTO.DeviceModule;
using SeguridadCiudadana.Services.Application.MainContext.DTO.AlertModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using SeguridadCiudadana.Services.Application.MainContext.DTO.PersonModule;

namespace SeguridadCiudadana.Services.DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [ApplicationErrorHandlerAttribute()] // manage all unhandled exceptions
    //[UnityInstanceProviderServiceBehavior()] //create instance and inject dependencies using unity container
    public class AlertaModuleService : IAlertaModuleService
    {
        #region Members

        readonly ISecurityAppService _securityAppService;
        readonly IAlertAppService _alertAppService;

        #endregion

        #region Constructor

        public AlertaModuleService(ISecurityAppService securityAppService, IAlertAppService alertAppService)
        {
            if (securityAppService == null)
                throw new ArgumentNullException("securityAppService");
            if (alertAppService == null)
                throw new ArgumentNullException("alertAppService");

            this._securityAppService = securityAppService;
            this._alertAppService = alertAppService;
        }
        #endregion

        #region IAlertaModuleService Members

        public string CheckDevice(string phoneNumber, string securityCode)
        {
            string respuesta = "OK";

            if(!this._alertAppService.IsDeviceExists(phoneNumber))
            {
                respuesta = "El Número Telefónico es inválido";
            }
            else
            {
                var dispositivo = this._alertAppService.GetDeviceByPhoneNumber(phoneNumber);
                if (!this._securityAppService.IsUserActive(dispositivo.UserId))
                {
                    respuesta = "El Usuario esta bloqueado";
                }
                else
                    if (!this._alertAppService.IsSecureCodeValid(dispositivo.DispositivoId, securityCode))
                    {
                        respuesta = "El Código de Seguridad es inválido";
                    }

            }
            
            return respuesta;
        }

        public DispositivoDTO RegisterDevice(string phoneNumber, string securityCode)
        {
            return this._alertAppService.RegisterDevice(phoneNumber, securityCode);
        }

        public bool SendAlert(string latitud, string longitud, string direccion, int tipo_alerta, int dispositivoId)
        {
            bool retorno = false;
            retorno = this._alertAppService.SendAlert(latitud, longitud, direccion, tipo_alerta, dispositivoId);
            return retorno;
        }

        public PersonaDTO CreatePerson(string apellido, string nombre, int documento, string fecha_nacimiento, string telefono_celular, string telefono_opcional, string email)
        {
            return _securityAppService.CreatePerson( apellido,  nombre,  documento,  fecha_nacimiento,  telefono_celular,  telefono_opcional,  email);
        }

        public PersonaDTO UpdatePerson(int documento)
        {
            return _securityAppService.UpdatePerson(documento);
        }

        #endregion

        public void Dispose()
        {
            //dispose all resources
            this._securityAppService.Dispose();
            this._alertAppService.Dispose();
        }

      
    }
}



