using Microsoft.Practices.Unity;
using SeguridadCiudadana.Fwk.Application.Core.Services;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Adapter;
using SeguridadCiudadana.Fwk.Infrastructure.Data.Core;
using SeguridadCiudadana.Services.Application.MainContext.DTO;
using Unity.Wcf;

namespace SeguridadCiudadana.Services.DistributedServices.MainBoundedContext
{
	public class WcfServiceFactory : UnityServiceHostFactory
    {
        protected override void ConfigureContainer(IUnityContainer container)
        {
            // register all your components with the container here
            // container
            //    .RegisterType<IService1, Service1>()
            //    .RegisterType<DataContext>(new HierarchicalLifetimeManager());
            //container.RegisterType<IConfigurationAppService, ConfigurationAppService>();

            container.RegisterType<IAlertaModuleService, AlertaModuleService>();

            //-> Unit of Work 
            container.RegisterType<IQueryableUnitOfWork, SeguridadCiudadana.Fwk.Infrastructure.Data.Core.SeguridadCiudadanaEntities>(new PerResolveLifetimeManager(), new InjectionConstructor());

            //-> Repositories
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new TransientLifetimeManager());

            //-> Adapters
            //_container.RegisterType<IPrintingService, PrintingService>(new TransientLifetimeManager());

            //-> Application services
            container.RegisterType<IConfigurationAppService, ConfigurationAppService>(new TransientLifetimeManager());
            container.RegisterType<ISecurityAppService, SecurityAppService>(new TransientLifetimeManager());
            container.RegisterType<IAlertAppService, AlertAppService>(new TransientLifetimeManager());

            //-> Adapters
            container.RegisterType<ITypeAdapterFactory, AutomapperTypeAdapterFactory>(new ContainerControlledLifetimeManager());


            //Configure Factories
            var typeAdapterFactory = container.Resolve<ITypeAdapterFactory>();
            TypeAdapterFactory.SetCurrent(typeAdapterFactory);

            

        }
    }    
}