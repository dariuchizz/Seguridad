﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SeguridadCiudadana.Services.DistributedServices.MainBoundedContext.Tests
{
    [TestClass]
    public class AlertaModuleServiceTests
    {
        

        [TestMethod]
        public void FindCountries()
        {
            //Arrange
            DGS.Services.DistributedServices.MainBoundedContext.Tests.AlertaModuleSvc.AlertaAppServiceClient client = new DGS.Services.DistributedServices.MainBoundedContext.Tests.AlertaModuleSvc.AlertaAppServiceClient();

            //Act
            var result = client.GetData(1);

            //Assert
            Assert.IsTrue(result.Any());
        }
    }
}
