﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Services.Application.MainContext.DTO.AlertModule
{
    public class AlertDTO
    {
        public string Latitud { get; set; }
        public string Longitud { get; set; }
     
        public string Direccion { get; set; }
        public Nullable<int> TipoAlerta { get; set; }
        public int DispositivoId { get; set; }
    }
}
