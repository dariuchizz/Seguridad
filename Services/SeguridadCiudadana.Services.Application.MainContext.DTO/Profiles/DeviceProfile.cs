﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Services.Application.MainContext.DTO.DeviceModule;

namespace SeguridadCiudadana.Services.Application.MainContext.DTO.Profiles
{
    public class DeviceProfile : Profile
    {
        protected override void Configure()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Dispositivo, DispositivoDTO>()
                .ForMember(dto => dto.DispositivoId, mc => mc.MapFrom(e => e.DispositivoId))
                .ForMember(dto => dto.CodigoSeguridad, mc => mc.MapFrom(e => e.CodigoSeguridad))
                .ForMember(dto => dto.NumeroTelefonico, mc => mc.MapFrom(e => e.NumeroTelefonico))
               // .ForMember(dto => dto.Titular, mc => mc.MapFrom(e => e.AspNetUsers.Persona.FirstOrDefault().NombreCompleto))
                .ForMember(dto => dto.UserId, mc => mc.MapFrom(e => e.UserId));
            });

            IMapper mapper = config.CreateMapper();


        }
    }
}
