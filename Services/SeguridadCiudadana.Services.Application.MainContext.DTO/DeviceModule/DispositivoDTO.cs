﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Services.Application.MainContext.DTO.DeviceModule
{
    public class DispositivoDTO
    {
        public int DispositivoId { get; set; }
        public string NumeroTelefonico { get; set; }
        public string CodigoSeguridad { get; set; }
        public string UserId { get; set; }
        public string Titular { get; set; }
        public string Mensaje { get; set; }
        public int Estado { get; set; }

    }
}
