﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Services.Application.MainContext.DTO.PersonModule
{
    public class PersonaDTO
    {
        public string Apellido { get; set; }
        public string Nombre { get; set; }                
        public int Documento { get; set; }
        public string FechaNacimiento { get; set; }       
        public string TelefonoCelular { get; set; }
        public string TelefonoOpcional { get; set; }
        public string Email { get; set; }
        public string Mensaje { get; set; }
    }
}
