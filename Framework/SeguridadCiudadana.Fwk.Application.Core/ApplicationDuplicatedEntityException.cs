﻿using SeguridadCiudadana.Fwk.Application.Core.Resources;
using SeguridadCiudadana.Fwk.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Application.Core
{
    public class ApplicationDuplicatedEntityException : Exception
    {
        #region Properties

        private EntityBase _Entity;

        public EntityBase Entity
        {
            get
            {
                return this._Entity;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Create new instance of Application validation errors exception
        /// </summary>
        /// <param name="validationErrors">The collection of validation errors</param>
        public ApplicationDuplicatedEntityException(EntityBase entity)
            : base(Messages.exception_ApplicationDuplicatedEntityExceptionDefaultMessage)
        {
            this._Entity = entity;
        }

        #endregion 
    }
}
