﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using DGS.Services.Application.MainContext.DTO.SecurityModule;
using DGS.Services.Domain.MainContext.SecurityModule;
using DGS.Services.Domain.MainContext.SecurityModule.Aggregates.ApplicationAgg;
using DGS.Services.Domain.MainContext.SecurityModule.Aggregates.UserAgg;
using DGS.Services.Domain.MainContext.SecurityModule.Aggregates.LogAgg;

namespace DGS.Services.Application.MainContext.DTO.Profiles
{
    public class SecurityProfile : Profile
    {
        protected override void Configure()
        {
            var userMappingExpression = Mapper.CreateMap<User, UserDTO>();

            var userLisMappingExpression = Mapper.CreateMap<User, UserListDTO>();

            var roleMappingExpression = Mapper.CreateMap<Role, RoleDTO>();
            roleMappingExpression.ForMember(dto => dto.Status,
                mc => mc.MapFrom(e => (Status)e.Status));

            var roleListMappingExpression = Mapper.CreateMap<Role, RoleListDTO>();
            roleListMappingExpression.ForMember(dto => dto.Status,
                mc => mc.MapFrom(e => (Status)e.Status));
            roleListMappingExpression.ForMember(dto => dto.ApplicationName,
                mc => mc.MapFrom(e => e.Application.Name));

            var menuMappingExpression = Mapper.CreateMap<Menu, MenuDTO>();
            menuMappingExpression.ForMember(dto => dto.Status,
                mc => mc.MapFrom(e => (Status)e.Status));
            menuMappingExpression.ForMember(dto => dto.ParentMenuId,
                mc => mc.MapFrom(e => e.ParentMenuId ?? 0));
            menuMappingExpression.ForMember(dto => dto.ParentMenuName,
                mc => mc.MapFrom(e => e.ParentMenu != null ? e.ParentMenu.Name : null));
            menuMappingExpression.ForMember(dto => dto.PathMenu,
                mc => mc.MapFrom(e => Menu.GetPathMenu(e.ParentMenu, string.Empty) + e.Name));

            var applicaionMappingExpression = Mapper.CreateMap<DGSApplication, DGSApplicaionDTO>();
            applicaionMappingExpression.ForMember(dto => dto.Status,
                 mc => mc.MapFrom(e => (Status)e.Status));
            applicaionMappingExpression.ForMember(dto => dto.Type,
                 mc => mc.MapFrom(e => (DGSApplicationType)e.Type));

            var logMappingExpression = Mapper.CreateMap<Log, LogDTO>();
            logMappingExpression.ForMember(dto => dto.Type, mc => mc.MapFrom(e => (LogType)e.LogTypeId));
           
            Mapper.CreateMap<ActionType, ActionTypeDTO>();
            Mapper.CreateMap<LogDetail, LogDetailDTO>();
        }
    }
}
