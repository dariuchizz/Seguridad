﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using DGS.Services.Application.MainContext.DTO.ConfiguracionModule;
using DGS.Services.Domain.MainContext.ConfiguracionModule;

namespace SeguridadCiudadana.Services.Application.MainContext.DTO.Profiles
{
    public class ConfiguracionProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Periodo, PeriodoDTO>();
            Mapper.CreateMap<GrupoAdicional, GrupoAdicionalDTO>();
            Mapper.CreateMap<TipoLiquidacion, TipoLiquidacionDTO>();
            var repMappingExpression = Mapper.CreateMap<Reparticion, ReparticionDTO>();
            repMappingExpression.ForMember(dto => dto.DescLarga, mc => mc.MapFrom(e => e.Id + "-" + e.Descripcion));
            Mapper.CreateMap<GrupoReparticion, GrupoReparticionDTO>();
            Mapper.CreateMap<Turno, TurnoDTO>();

            var tipoHojaMappingExpression = Mapper.CreateMap<TipoHoja, TipoHojaDTO>();
            tipoHojaMappingExpression.ForMember(dto => dto.Externa, mc => mc.MapFrom(e => e.Externa > 0 ? true : false));

            Mapper.CreateMap<Provincia, ProvinciaDTO>();
            Mapper.CreateMap<EstadoHoja, EstadoHojaDTO>();
            Mapper.CreateMap<TipoEmpleo, TipoEmpleoDTO>();

            var localidadMappingExpression = Mapper.CreateMap<Localidad, LocalidadDTO>();
            localidadMappingExpression.ForMember(dto => dto.Provincia, mc => mc.MapFrom(e => e.Provincia.Descripcion));
            localidadMappingExpression.ForMember(dto => dto.ProvinciaId, mc => mc.MapFrom(e => e.Provincia.Id));

            Mapper.CreateMap<Persona, PersonaDTO>();
            Mapper.CreateMap<TipoHoja, TipoHojaDTO>();
        }
    }
}
