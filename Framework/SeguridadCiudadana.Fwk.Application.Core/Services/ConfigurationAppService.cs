﻿//using SeguridadCiudadana.Fwk.Application.Core.Cache;
using SeguridadCiudadana.Fwk.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Application.Core.Services
{
    public class ConfigurationAppService : IConfigurationAppService
    {
        #region Members
        private readonly IRepository<Pais> _countryRepository;
        private readonly IRepository<Provincia> _stateRepository;
        private readonly IRepository<Localidad> _cityRepository;

        #endregion

        #region Constructor
        public ConfigurationAppService(IRepository<Pais> countryRepository, IRepository<Provincia> stateRepository, IRepository<Localidad> cityRepository)
        {
            if (countryRepository == null)
                throw new ArgumentNullException("countryRepository");
            if (stateRepository == null)
                throw new ArgumentNullException("stateRepository");
            if (cityRepository == null)
                throw new ArgumentNullException("cityRepository");


            this._countryRepository = countryRepository;
            this._stateRepository = stateRepository;
            this._cityRepository  = cityRepository;

        }
        #endregion

        #region IConfigurationAppService Members
        public IEnumerable<Pais> FindCountries()
        {
            return this._countryRepository.GetAll().OrderBy(c => c.PaisId);
        }

        public IEnumerable<Provincia> FindStatesByCountryId(int countryId)
        {
            var country = this._countryRepository.Get(countryId);

            if (country != null)
            {
                return country.Provincia.OrderBy(s => s.ProvinciaId);
            }

            return null;
        }

        public IEnumerable<Localidad> FindCitiesByStateId(int stateId)
        {
            var state = this._stateRepository.Get(stateId);

            if (state != null)
            {
                return state.Localidades.OrderBy(s => s.LocalidadId);
            }

            return null;
        }

        public Localidad GetLocalidadById(int id)
        {
            return this._cityRepository.Get(id);
        }


        public void Dispose()
        {
            this._countryRepository.Dispose();
            
        }
        #endregion

        
    }
}
