﻿using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Services.Application.MainContext.DTO.DeviceModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeguridadCiudadana.Services.Application.MainContext.DTO.AlertModule;

namespace SeguridadCiudadana.Fwk.Application.Core.Services
{
    public interface IAlertAppService : IDisposable
    {
        bool IsDeviceExists(string phoneNumber);
        
        bool IsSecureCodeValid(int deviceId, string code);

        Dispositivo GetDeviceByPhoneNumber(string phoneNumber);

        IEnumerable<Dispositivo> GetAllDevices();

        IEnumerable<Dispositivo> GetDevicesByUserId(string userId);

        Dispositivo GetDeviceById(int id);

        void CreateDevice(Dispositivo device);

        void EditDevice(Dispositivo device);

        void DeleteDevice(Dispositivo device);

        IEnumerable<Alerta> GetTopFiveAlerts();

        Alerta GetAlertById(int id);

        IEnumerable<Alerta> GetAlertsById(int id);

        void CreateAlertHistory(AlertaHistoria alertHistory);

        DispositivoDTO RegisterDevice(string phoneNumber, string securityCode);

        bool SendAlert(string latitud, string longitud, string direccion, int tipo_alerta, int dispositivoId);
    }
}
