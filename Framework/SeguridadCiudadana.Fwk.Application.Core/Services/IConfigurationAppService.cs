﻿using SeguridadCiudadana.Fwk.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Application.Core.Services
{
    public interface IConfigurationAppService : IDisposable
    {
        IEnumerable<Pais> FindCountries();
        IEnumerable<Provincia> FindStatesByCountryId(int countryId);
        IEnumerable<Localidad> FindCitiesByStateId(int stateId);

        Localidad GetLocalidadById(int id);

    }
}
