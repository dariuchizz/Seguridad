﻿//using SeguridadCiudadana.Fwk.Application.Core.Cache;
using SeguridadCiudadana.Fwk.Application.Core.Resources;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Services.Application.MainContext.DTO.DeviceModule;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Validator;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SeguridadCiudadana.Fwk.Application.Core.Services
{
    public class AlertAppService : IAlertAppService
    {
        #region Members
        private readonly IRepository<Dispositivo> _deviceRepository;
        private readonly IRepository<Alerta> _alertRepository;
        private readonly IRepository<AlertaHistoria> _alertHistoryRepository;

        private readonly ISecurityAppService _securityAppService;
       
        #endregion

        #region Constructor
        public AlertAppService(IRepository<Dispositivo> deviceRepository, ISecurityAppService securityAppService, IRepository<Alerta> alertRepository, IRepository<AlertaHistoria> alertHistoryRepository)
        {
            if (deviceRepository == null)
                throw new ArgumentNullException("deviceRepository");

            if (alertRepository == null)
                throw new ArgumentNullException("alertRepository");

            if (alertHistoryRepository == null)
                throw new ArgumentNullException("alertHistoryRepository");
            if (securityAppService == null)
                throw new ArgumentNullException("securityAppService");
            if (alertRepository == null)
                throw new ArgumentException("alertRepository");

            this._deviceRepository = deviceRepository;
            this._alertRepository = alertRepository;
            this._alertHistoryRepository = alertHistoryRepository;

            this._securityAppService = securityAppService;
           

        }
        
        #endregion

        #region IAlertAppService Members

        public bool IsDeviceExists(string phoneNumber)
        {
            
            bool retorno = false;

            if (this._deviceRepository.GetFiltered(x => x.NumeroTelefonico == phoneNumber).Count() > 0)
            {
                retorno = true;
            }
            return retorno;

        }

        public bool IsSecureCodeValid(int deviceId, string code)
        {
            bool retorno = false;

            if (this._deviceRepository.GetFiltered(x=> x.DispositivoId == deviceId && x.CodigoSeguridad == code).Count() > 0)
            {
                retorno = true;
            }
            return retorno;
        }

        public void Dispose()
        {
            this._deviceRepository.Dispose();
            
        }

        public Dispositivo GetDeviceByPhoneNumber(string phoneNumber)
        {
            return this._deviceRepository.GetFiltered(x => x.NumeroTelefonico == phoneNumber).FirstOrDefault();

        }

        //public IEnumerable<Dispositivo> GetAllDevices()
        //{
        //    return this._deviceRepository.GetAll().OrderBy(c => c.DispositivoId);
        //}

        //public IEnumerable<Dispositivo> GetDevicesByUserId(string userId)
        //{
        //    return this._deviceRepository.GetAll().Where(x=> x.AspNetUsers.Id == userId) .OrderBy(c => c.DispositivoId);
        //}

        public Dispositivo GetDeviceById(int id)
        {
            return this._deviceRepository.Get(id);
        }

        public void CreateDevice(Dispositivo device)
        {
            if (device == null)
                throw new ArgumentException(Messages.warning_CannotAddRolWithEmptyInformation);

            //var validator = EntityValidatorFactory.CreateValidator();

            //if (validator.IsValid<Dispositivo, DispositivoValidation>(device))
            //{

            //    this._deviceRepository.Add(device);
            //    this._deviceRepository.UnitOfWork.Commit();
            //}
            //else
            //    throw new ApplicationValidationErrorsException(validator.GetInvalidMessages<Dispositivo>(device));
            this._deviceRepository.Add(device);
            this._deviceRepository.UnitOfWork.Commit();
        }

        public void EditDevice(Dispositivo device)
        {

            this._deviceRepository.Modify(device);
            this._deviceRepository.UnitOfWork.Commit();
        }
        
        public void DeleteDevice(Dispositivo device)
        {

            this._deviceRepository.Remove(device);
            this._deviceRepository.UnitOfWork.Commit();
        }

        //public IEnumerable<Alerta> GetTopFiveAlerts()
        //{
        //    return this._alertRepository.GetAll().OrderByDescending(c => c.FechaHora).Take(5);
        //}

        public Alerta GetAlertById(int id)
        {
            return this._alertRepository.Get(id);
        }

        public DispositivoDTO RegisterDevice(string phoneNumber, string securityCode)
        {
            var dtoEmpty = new DispositivoDTO();

            if (this.IsDeviceExists(phoneNumber))
            {
                var dispositivo = this.GetDeviceByPhoneNumber(phoneNumber);
                if (this._securityAppService.IsUserActive(dispositivo.UserId))
                {
                    if (this.IsSecureCodeValid(dispositivo.DispositivoId, securityCode))
                    {
                        return new DispositivoDTO()
                        {
                            DispositivoId = dispositivo.DispositivoId,
                            NumeroTelefonico = dispositivo.NumeroTelefonico,
                            CodigoSeguridad = dispositivo.CodigoSeguridad,
                            UserId = dispositivo.UserId,
                            Titular = dispositivo.AspNetUsers.Persona.FirstOrDefault().NombreCompleto,
                            Mensaje = "Bienvenido, usted se encuentra registrado. Gracias!",
                            Estado = 1    
                        };
                    }
                    else
                    {
                        dtoEmpty.Mensaje = "codigo de seguridad invalido";
                        dtoEmpty.Estado = 2;
                        return dtoEmpty;//codigo de seguridad invalido
                    }
                }
                else
                {
                    dtoEmpty.Mensaje = "usuario inactivo";
                    dtoEmpty.Estado = 3;
                    return dtoEmpty;//usuario inactivo
                }
            }
            else
            {
                dtoEmpty.Mensaje = "telefono invalido";
                dtoEmpty.Estado = 4;
                return dtoEmpty;//telefono invalido

            }
            
        }

        //public IEnumerable<Alerta> GetAlertsById(int id)
        //{
        //    return this._alertRepository.GetFiltered(x => x.AlertaId == id);

        //}

        public void CreateAlertHistory(AlertaHistoria alertHistory)
        {
            if (alertHistory == null)
                throw new ArgumentException(Messages.warning_CannotAddRolWithEmptyInformation);

            //var validator = EntityValidatorFactory.CreateValidator();

            //if (validator.IsValid<Dispositivo, DispositivoValidation>(device))
            //{

            //    this._deviceRepository.Add(device);
            //    this._deviceRepository.UnitOfWork.Commit();
            //}
            //else
            //    throw new ApplicationValidationErrorsException(validator.GetInvalidMessages<Dispositivo>(device));
            this._alertHistoryRepository.Add(alertHistory);
            this._deviceRepository.UnitOfWork.Commit();
        }
        public bool SendAlert(string latitud, string longitud, string direccion, int tipo_alerta, int dispositivoId)
        {
            bool retorno = false;
            //get Id del usuario a partir del dispositivoId
            string userId = _deviceRepository.GetFiltered(x => x.DispositivoId == dispositivoId).First().UserId;

            Alerta _alertEntity = new Alerta
            {
                Latitud = latitud,
                Longitud = longitud,
                FechaHora = DateTime.Now,
                //Direccion = direccion,
                TipoAlerta = tipo_alerta,
                DispositivoId = dispositivoId
            };

            AlertaHistoria _alertHistoryEntity = new AlertaHistoria
            {
                Estado = 1,
                FechaHora = DateTime.Now,
                UserId = userId
            };
            
            try {
                _alertEntity.AlertasHistoria.Add(_alertHistoryEntity);
                _alertRepository.Add(_alertEntity);
                _alertRepository.UnitOfWork.Commit();
                retorno = true;
            } catch (Exception ex) {               
                throw;
            }
            return retorno;
        }

        public IEnumerable<Dispositivo> GetAllDevices()
        {
            return this._deviceRepository.GetAll().OrderBy(c => c.DispositivoId);
        }

        public IEnumerable<Dispositivo> GetDevicesByUserId(string userId)
        {
            return this._deviceRepository.GetAll().Where(x => x.AspNetUsers.Id == userId).OrderBy(c => c.DispositivoId);
        }

        public IEnumerable<Alerta> GetTopFiveAlerts()
        {
            return this._alertRepository.GetAll().OrderByDescending(c => c.FechaHora).Take(5);
        }

        public IEnumerable<Alerta> GetAlertsById(int id)
        {
            return this._alertRepository.GetFiltered(x => x.AlertaId == id);
        }

        #endregion


    }
}
