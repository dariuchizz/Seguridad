﻿//using SeguridadCiudadana.Fwk.Application.Core.Cache;
using SeguridadCiudadana.Fwk.Application.Core.Resources;
using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Validator;
using SeguridadCiudadana.Services.Application.MainContext.DTO.PersonModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Application.Core.Services
{
    public class SecurityAppService : ISecurityAppService
    {
        #region Members
        private readonly IRepository<AspNetRoles> _roleRepository;
        private readonly IRepository<AspNetUsers> _userRepository;
        private readonly IRepository<Persona> _personRepository;
        private readonly IRepository<AspNetUserClaims> _claimRepository;

        #endregion

        #region Constructor
        public SecurityAppService(IRepository<AspNetRoles> roleRepository, IRepository<AspNetUsers> userRepository, IRepository<Persona> personRepository, IRepository<AspNetUserClaims> claimRepository)
        {
            if (roleRepository == null)
                throw new ArgumentNullException("roleRepository");
            if (userRepository == null)
                throw new ArgumentNullException("userRepository");
            if (personRepository == null)
                throw new ArgumentNullException("personRepository");
            if (claimRepository == null)
                throw new ArgumentNullException("claimRepository");


            this._roleRepository = roleRepository;

            this._userRepository = userRepository;

            this._personRepository = personRepository;

            this._claimRepository = claimRepository;

        }
        #endregion

        #region ISecurityAppService Members
        public IEnumerable<AspNetRoles> GetAllRoles()
        {
            return this._roleRepository.GetAll().OrderBy(c => c.Id);
        }

        public AspNetRoles GetRolById(int id)
        {
            return this._roleRepository.Get(id);
        }

        public void CreateRol(AspNetRoles rol)
        {
            if (rol == null)
                throw new ArgumentException(Messages.warning_CannotAddRolWithEmptyInformation);

            this._roleRepository.Add(rol);
            this._roleRepository.UnitOfWork.Commit();

            //var validator = EntityValidatorFactory.CreateValidator();

            //if (validator.IsValid<AspNetRoles, AspNetRolesValidation>(rol))
            //{

            //    this._roleRepository.Add(rol);
            //    this._roleRepository.UnitOfWork.Commit();
            //}
            //else
            //    throw new ApplicationValidationErrorsException(validator.GetInvalidMessages<AspNetRoles>(rol));

        }

        public void EditRol(AspNetRoles rol)
        {

            this._roleRepository.Modify(rol);
            this._roleRepository.UnitOfWork.Commit();
        }

        public void DeleteRol(AspNetRoles rol)
        {

            this._roleRepository.Remove(rol);
            this._roleRepository.UnitOfWork.Commit();
        }

        public IEnumerable<AspNetUsers> GetAllUsers()
        {
            return this._userRepository.GetAll().OrderBy(c => c.Id);
        }

        public AspNetUsers GetUserById(string id)
        {
            return this._userRepository.GetFiltered(x => x.Id == id).FirstOrDefault();
        }

        public void CreateUser(AspNetUsers user)
        {
            if (user == null)
                throw new ArgumentException(Messages.warning_CannotAddRolWithEmptyInformation);

            var validator = EntityValidatorFactory.CreateValidator();

            if (validator.IsValid<AspNetUsers, AspNetUsersValidation>(user))
            {

                this._userRepository.Add(user);
                this._userRepository.UnitOfWork.Commit();
            }
            else
                throw new ApplicationValidationErrorsException(validator.GetInvalidMessages<AspNetUsers>(user));

        }

        public void EditUser(AspNetUsers user)
        {
            try
            {

                this._userRepository.Modify(user);
                this._userRepository.UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        
        public void DeleteUser(AspNetUsers user)
        {

            this._userRepository.Remove(user);
            this._userRepository.UnitOfWork.Commit();
        }

        public bool IsUserActive(string userId)
        {

            bool retorno = false;

            if (this._userRepository.GetFiltered(x => x.Id == userId && !x.LockoutEnabled ).Count() > 0)
            {
                retorno = true;
            }
            return retorno;

        }

        public AspNetUsers GetUserByName(string id)
        {
            return this._userRepository.GetFiltered(x => x.UserName  == id).FirstOrDefault();
        }

        public void Dispose()
        {
            this._roleRepository.Dispose();
            this._userRepository.Dispose();
        }

        public PersonaDTO CreatePerson(string apellido, string nombre, int documento, string fecha_nacimiento, string telefono_celular, string telefono_opcional, string email)
        {
            string retorno = null;
            int dni = documento;
            try
            {
                if (!IsPersonExists(dni))
                {
                    DateTime fechaNac = Convert.ToDateTime(fecha_nacimiento);
                    Persona personaEntity = new Persona
                    {
                        Apellido = apellido,
                        Nombre = nombre,
                        Documento = documento,
                        FechaNacimiento = fechaNac,
                        FechaRegistracion = DateTime.Now.Date,
                        TelefonoCelular = telefono_celular,
                        TelefonoOpcional = telefono_opcional,
                        Email = email
                    };
                    _personRepository.Add(personaEntity);
                    _personRepository.UnitOfWork.Commit();

                    return new PersonaDTO()
                    {                                           
                        Apellido = apellido,
                        Nombre = nombre,
                        Documento = documento,
                        FechaNacimiento = fecha_nacimiento,                        
                        TelefonoCelular = telefono_celular,
                        TelefonoOpcional = telefono_opcional,
                        Mensaje = "Bienvenido, usted se encuentra registrado. Gracias!"                        
                    };
                }
                else {
                    return new PersonaDTO()
                    {                        
                        Mensaje = "Usted ya se encontraba registrado en nuestro sistema. Gracias."
                    };
                }
            }
            catch (Exception ex) { throw; }
        }

        public bool IsPersonExists(int document)
        {
            return _personRepository.GetFiltered(x => x.Documento == document).Any();
        }

        public PersonaDTO UpdatePerson(int document)
        {
            if (IsPersonExists(document))
            {
                Persona entityPerson = _personRepository.GetFiltered(x => x.Documento == document).FirstOrDefault();
                return new PersonaDTO
                {
                    Apellido = entityPerson.Apellido,
                    Nombre = entityPerson.Nombre,
                    Documento = document,
                    FechaNacimiento = entityPerson.FechaNacimiento.ToString(),
                    TelefonoCelular = entityPerson.TelefonoCelular,
                    TelefonoOpcional = entityPerson.TelefonoOpcional,
                    Mensaje = "Bienvenido, usted se encuentra registrado. Gracias!"
                };
            }
            else {
                return new PersonaDTO
                {                    
                    Mensaje = "Usted no se encuentra registrado en nuestro sistema."
                };
            }
        }

        public void EditUserClaim(AspNetUserClaims claim)
        {
            try
            {

                this._claimRepository.Modify(claim);
                this._claimRepository.UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public AspNetUserClaims GetUserClaimByUserId(string id, string type)
        {
            return this._claimRepository.GetFiltered(x => x.UserId == id && x.ClaimType == type).FirstOrDefault();

        }
        
        public IEnumerable<Persona> GetAllPersons()
        {
            return this._personRepository.GetAll().OrderBy(c => c.Apellido);
        }

        #endregion
    }

}