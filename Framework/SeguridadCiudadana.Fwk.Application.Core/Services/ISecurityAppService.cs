﻿using SeguridadCiudadana.Fwk.Domain.Core;
using SeguridadCiudadana.Services.Application.MainContext.DTO.PersonModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Application.Core.Services
{
    public interface ISecurityAppService : IDisposable
    {
        IEnumerable<AspNetRoles> GetAllRoles();

        AspNetRoles GetRolById(int id);

        void CreateRol(AspNetRoles rol);

        void EditRol(AspNetRoles rol);

        void DeleteRol(AspNetRoles rol);

        IEnumerable<AspNetUsers> GetAllUsers();

        AspNetUsers GetUserById(string id);

        void CreateUser(AspNetUsers rol);

        void EditUser(AspNetUsers rol);
        
        void DeleteUser(AspNetUsers user);

        bool IsUserActive(string userId);

        AspNetUsers GetUserByName(string id);

        PersonaDTO CreatePerson(string apellido, string nombre, int documento, string fecha_nacimiento, string telefono_celular, string telefono_opcional, string email);

        bool IsPersonExists(int document);

        PersonaDTO UpdatePerson(int document);

        void EditUserClaim(AspNetUserClaims claim);

        AspNetUserClaims GetUserClaimByUserId(string id, string type);

        IEnumerable<Persona> GetAllPersons();

    }
}
