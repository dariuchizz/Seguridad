﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Constants
{
    public static class RolConst
    {
        public const string CLIENTE = "Cliente";
        public const string OPERADOR = "Operador";
        public const string ADMINISTRADOR = "Administrador";
        
    }
}
