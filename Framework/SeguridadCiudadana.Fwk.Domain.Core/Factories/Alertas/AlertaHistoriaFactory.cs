﻿using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using SeguridadCiudadana.Fwk.Domain.Core.States.Alertas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Factories.Alertas
{
    public class AlertaHistoriaFactory
    {
        public static AlertaHistoria CreateAlertaHistoria(int estado, DateTime fechahora, int alertaId, string userId, string observacion)
        {
            var alertaHistoria = new AlertaHistoria
            {
                Estado = estado,
                FechaHora = fechahora,
                AlertaId = alertaId,
                UserId = userId,
                Observacion = observacion
                
            };
            alertaHistoria.CurrentState = AlertaStateFactory.CreateState((Estado)alertaHistoria.Estado);
            return alertaHistoria;

        }

        
    }
}
