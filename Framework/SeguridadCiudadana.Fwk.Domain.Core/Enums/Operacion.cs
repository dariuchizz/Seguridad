﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Enums
{
    public enum Operacion
    {
        Tomar = 2,
        Rechazar = 3,
        Resolver = 4,
        Suspender = 5

    }
}
