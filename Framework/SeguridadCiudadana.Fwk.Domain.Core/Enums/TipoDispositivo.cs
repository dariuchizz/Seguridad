﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Enums
{
    public enum TipoDispositivo : int
    {
        ANDROID = 1,
        BOTON = 2
    }
}
