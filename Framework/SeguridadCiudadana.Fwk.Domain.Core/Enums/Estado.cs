﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Enums
{
    public enum Estado : int
    {
        Creado = 1,
        Asignado = 2,
        Rechazado = 3,
        Resuelto = 4,
        Suspendido = 5
    }
}
