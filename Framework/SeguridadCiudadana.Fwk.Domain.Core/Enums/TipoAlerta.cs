﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Enums
{
    public enum TipoAlerta : int
    {
        Salud = 1,
        Peligro = 2,
        Bombero = 3

    }
}
