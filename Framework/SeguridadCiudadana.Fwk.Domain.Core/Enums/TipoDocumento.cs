﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Enums
{
    public enum TipoDocumento : int
    {
        DNI = 1,
        CI = 2,
        LC=3,
        LD=4
    }
}
