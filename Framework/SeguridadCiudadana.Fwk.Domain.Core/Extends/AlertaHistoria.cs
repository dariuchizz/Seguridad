﻿using DGS.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using SeguridadCiudadana.Fwk.Domain.Core.Extends;
using SeguridadCiudadana.Fwk.Domain.Core.Resources;
using SeguridadCiudadana.Fwk.Domain.Core.States.Alertas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SeguridadCiudadana.Fwk.Domain.Core
{

    [MetadataType(typeof(AlertaHistoriaValidation))]
    public partial class AlertaHistoria 
    {
       
        public virtual void AAsignada()
        {
            Estado =(int) SeguridadCiudadana.Fwk.Domain.Core.Enums.Estado.Asignado;
        }
        public virtual void AResuelta()
        {
            Estado = (int)SeguridadCiudadana.Fwk.Domain.Core.Enums.Estado.Resuelto;
        }
        public virtual void ARechazada()
        {
            Estado = (int)SeguridadCiudadana.Fwk.Domain.Core.Enums.Estado.Rechazado;
        }
        public virtual void ASuspendida()
        {
            Estado = (int)SeguridadCiudadana.Fwk.Domain.Core.Enums.Estado.Suspendido;
        }

        
        private IState _currentState;

        // Override auto-implemented property with ordinary property
        // to provide specialized accessor behavior.
        public override IState CurrentState
        {
            get
            {
                return AlertaStateFactory.CreateState((Estado)Estado);
            }
            set
            {
                _currentState= AlertaStateFactory.CreateState((Estado)Estado);
            }
        }

    }
    public partial class AlertaHistoriaValidation
    {
        


    }
}
