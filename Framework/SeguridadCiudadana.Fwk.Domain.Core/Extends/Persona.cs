﻿using SeguridadCiudadana.Fwk.Domain.Core.Extends;
using SeguridadCiudadana.Fwk.Domain.Core.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SeguridadCiudadana.Fwk.Domain.Core
{
   
    [MetadataType(typeof(PersonaValidation))]
    public partial class Persona : IValidatableObject
    {
        public string Delete { get; set; }
        public string NombreCompleto
        {
            get
            {
                return Nombre + " " + Apellido ?? "";
            }
            set
            {
                Nombre = value;
                Apellido = "";
            }
        }

        public HttpPostedFileBase Imagen { get; set; }
        public string ImagenBase64 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            if (string.IsNullOrEmpty(Email))
                yield return new ValidationResult("Este campo es obligatorio.", new[] { "Email" });

            if (string.IsNullOrEmpty(Apellido))
                yield return new ValidationResult("Este campo es obligatorio.", new[] { "Apellido" });

        }

    }
    public partial class PersonaValidation
    {
        public int PersonaId { get; set; }
        
        [Required(ErrorMessage = "El/Los Nombre/s es/son obligatorios.")]
        [DisplayName("Nombre/s")]
        public string Nombre { get; set; }
        
        [Required(ErrorMessage = "El Apellido es obligatorio.")]
        public string Apellido { get; set; }


        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DisplayName("Tipo de Documento")]
        public int TipoDocumento { get; set; }

        [Required(ErrorMessage = "El Documento es obligatorio.")]
        //[StringLength(8, MinimumLength = 1, ErrorMessage = "Número de Documento Inválido")]
        public string Documento { get; set; }

        [Required(ErrorMessage = "La Fecha de Nacimiento es obligatoria.")]
        [BirthDateValidationAttribute(ErrorMessage = "Fecha nacimiento no valida")]
        [Display(Name = "Fecha nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "Domicilio (Calle / Nro / Piso / Dpto. / Manzana)")]
        public string Calle { get; set; }

        //[Required(ErrorMessage = "El Código Postal es obligatorio.")]
        [Display(Name = "Código postal")]
        //[DataType(DataType.Text)]
        //[RegularExpression("\\d{5}", ErrorMessage = "El formato del Código Postal es inválido")]
        public string CodigoPostal { get; set; }

        [Required(ErrorMessage = "El Telefono Celular  es obligatorio.")]
        [Display(Name = "Teléfono Celular")]
        //[DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "El formato del Telefono es inválido. Ejemplo (381) 156 257896")]
        public string TelefonoCelular { get; set; }

        [Display(Name = "Teléfono Opcional")]
        //[DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$", ErrorMessage = "El formato del Telefono es inválido. Ejemplo (381) 156 257896")]          
        public string TelefonoOpcional { get; set; }

        [DisplayName("Pais")]
        //[Required(ErrorMessage = "El Pais es obligatorio.")]
        public int PaisId { get; set; }

        [DisplayName("Provincia")]
        //[Required(ErrorMessage = "La Provincia es obligatoria.")]
        public int ProvinciaId { get; set; }

        [DisplayName("Localidad")]
        //[Required(ErrorMessage = "La Localidad es obligatoria.")]
        public int LocalidadId { get; set; }

        public string Genero { get; set; }

    }
}
