﻿using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using SeguridadCiudadana.Fwk.Domain.Core.Extends;
using SeguridadCiudadana.Fwk.Domain.Core.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SeguridadCiudadana.Fwk.Domain.Core
{
    
    [MetadataType(typeof(DispositivoValidation))]
    public partial class Dispositivo : IValidatableObject
    {
        public string DescripcionLargaDispositivo
        {
            get
            {
                return Enum.GetName(typeof(TipoDispositivo), TipoDispositivo)  + NumeroTelefonico + " " + Marca + " " + Modelo + " " + Compania;
            }
            
        }

        public string DescripcionLargaCliente
        {
            get
            {
                return "Titular: "+this.AspNetUsers.Persona.FirstOrDefault().NombreCompleto;
            }

        }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            if (string.IsNullOrEmpty(NumeroTelefonico))
                yield return new ValidationResult("Este campo es obligatorio.", new[] { "NumeroTelefonico" });

        }

    }
    public partial class DispositivoValidation
    {
        [Required(ErrorMessage = "El Número Telefónico es obligatorio")]
        [DisplayName("Número Telefónico")]
        //[RegularExpression(@"/^((\+?34([ \t|\-])?)?[9|6|7]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/", ErrorMessage = "El formato del Telefono es inválido. +[código pais][código área] (sin '0') (sin '15')  Ej. +543819999999")]
        //[StringLength(7, MinimumLength = 1, ErrorMessage = "El formato del Número Telefonico es inválido. Debe ingresar el número sin el 15")]
        public string NumeroTelefonico { get; set; }

        [DisplayName("Tipo de Dispositivo")]
        public int TipoDispositivo { get; set; }

        [DisplayName("Compañia")]
        public string Compania { get; set; }


    }
}
