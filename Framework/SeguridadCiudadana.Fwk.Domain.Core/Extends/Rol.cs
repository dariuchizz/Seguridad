﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apuestas.Fwk.Domain.Core
{
    [MetadataType(typeof(RolMetadata))]
    public partial class Rol
    {
        #region Properties
        public override int Id
        {
            get
            {
                return this.RolId;
            }
            protected set
            {
                this.RolId = value;
            }
        }

        public override int SitioId
        {
            get
            {
                return this.Sitio;
            }
            protected set
            {
                this.Sitio = value;
            }
        }
        #endregion
    }

    public class RolMetadata
    {
        [Required]
        public string Descripcion { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Sitio no valido")]
        public int Sitio { get; set; }
    }
}
