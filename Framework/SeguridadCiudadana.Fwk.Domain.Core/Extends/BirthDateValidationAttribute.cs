﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.Extends
{
    public class BirthDateValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool respuesta = false;

            if(value != null)
            { 
                DateTime birthDate = (DateTime)value;
            
                DateTime today = DateTime.Now;
                DateTime validDate = new DateTime(today.Year - 18, today.Month, today.Day);
                TimeSpan validAge = today.Subtract(validDate);
                TimeSpan actualAge = today.Subtract(birthDate);
                int age = TimeSpan.Compare(validAge, actualAge);
                respuesta = age <= 0;
            }

            return respuesta ;
        }
    }
}
