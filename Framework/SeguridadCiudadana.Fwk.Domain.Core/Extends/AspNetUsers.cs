using System.ComponentModel;

namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System;
    using System.ComponentModel.DataAnnotations;
    [MetadataType(typeof(AspNetUsersValidation))]
    public partial class AspNetUsers
    {
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar password")]
        [Compare("Password", ErrorMessage = "Las contrase�as no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Soy mayor de 18 a�os, he le�do y acepto los T�rminos y condiciones.")]
        [Compare("IsTrue", ErrorMessage = "Por favor acepte los terminos y condiciones")]
        public bool AgreeTerms { get; set; }

        public bool IsTrue { get { return true; } }
    }

    public partial class AspNetUsersValidation
    {    
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        [DisplayName("Nombre de Usuario")]
        [Required(ErrorMessage = "El Nombre de Usuario  es obligatorio.")]
        public string UserName { get; set; }
    }
}
