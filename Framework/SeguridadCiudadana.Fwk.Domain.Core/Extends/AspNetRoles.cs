namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    [MetadataType(typeof(AspNetRolesValidation))]
    public partial class AspNetRoles
    {
        public bool IsSelected { get; set; }
    }

    public partial class AspNetRolesValidation
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio.")]
        public string Name { get; set; }
        public bool Activo { get; set; }

        
    }
}
