namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System.ComponentModel.DataAnnotations;
    [MetadataType(typeof(AspNetProfilesValidation))]
    public partial class AspNetProfiles
    {
        public bool IsSelected { get; set; }
    }

    public partial class AspNetProfilesValidation
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio.")]
        public string Name { get; set; }
        public bool Activo { get; set; }
    }
}
