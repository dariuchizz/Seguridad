﻿using DGS.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.States.Alertas
{
    public class AlertaCreada : IState
    {
        public void Change(EntityContext context, Enum operation)
        {
            var alerta = (AlertaHistoria) context;
            if (operation.Equals(Operacion.Tomar))
            {
                alerta.AAsignada();
            }

            alerta.CurrentState = AlertaStateFactory.CreateState((Estado)alerta.Estado);
        }

        public List<Enum> GetOperations(EntityContext context)
        {
            var alarta = (AlertaHistoria)context;

            return new List<Enum>()
            { Operacion.Tomar } ;
        }
    }
}
