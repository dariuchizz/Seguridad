﻿using DGS.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.States.Alertas
{
    public class AlertaSuspendida : IState
    {
        public void Change(EntityContext context, Enum operation)
        {
            var alerta = (AlertaHistoria)context;
            if (operation.Equals(Operacion.Tomar))
            {
                alerta.AAsignada();
            }
            if (operation.Equals(Operacion.Resolver))
            {
                alerta.AResuelta();
            }
            if (operation.Equals(Operacion.Rechazar))
            {
                alerta.ARechazada();
            }
            if (operation.Equals(Operacion.Suspender))
            {
                alerta.ASuspendida();
            }

            alerta.CurrentState = AlertaStateFactory.CreateState((Estado)alerta.Estado);
        }

        public List<Enum> GetOperations(EntityContext context)
        {
            var alarta = (AlertaHistoria)context;

            return new List<Enum>()
            { Operacion.Tomar, Operacion.Resolver, Operacion.Rechazar };
        }
    }
}
