﻿using DGS.Fwk.Domain.Core;
using SeguridadCiudadana.Fwk.Domain.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Domain.Core.States.Alertas
{
    public class AlertaStateFactory
    {
        public static IState CreateState(Estado estado)
        {
            switch (estado)
            {
                case Estado.Creado:
                    return new AlertaCreada();
                case Estado.Asignado:
                    return new AlertaAsignada();
                case Estado.Resuelto:
                    return new AlertaResuelta();
                case Estado.Rechazado:
                    return new AlertaRechazada();
                case Estado.Suspendido:
                    return new AlertaSuspendida();

                default:
                    return null;
            }
        }
    }
}
