﻿using SeguridadCiudadana.Fwk.Domain.Core;
using System;
using System.Collections.Generic;

namespace DGS.Fwk.Domain.Core
{
    public abstract class EntityContext : EntityBase
    {
        public virtual IState CurrentState { get; set; }

        public virtual void Request(Enum operation)
        {
            CurrentState.Change(this, operation);
        }

        public virtual List<Enum> GetOperations()
        {
            return CurrentState.GetOperations(this);
        }
    }
}
