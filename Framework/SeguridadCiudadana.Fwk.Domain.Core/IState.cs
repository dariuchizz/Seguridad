﻿using System;
using System.Collections.Generic;

namespace DGS.Fwk.Domain.Core
{
    public interface IState
    {
        void Change(EntityContext context, Enum operation);
        List<Enum> GetOperations(EntityContext context);
    }
}
