//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pais : EntityBase
    {
        public Pais()
        {
            this.Provincia = new HashSet<Provincia>();
            this.Personas = new HashSet<Persona>();
        }
    
        public int PaisId { get; set; }
        public string Descripcion { get; set; }
    
        public virtual ICollection<Provincia> Provincia { get; set; }
        public virtual ICollection<Persona> Personas { get; set; }
    }
}
