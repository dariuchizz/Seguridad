//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System;
    
    public enum TipoApuesta : int
    {
        Simple = 1,
        Combinada = 2
    }
}
