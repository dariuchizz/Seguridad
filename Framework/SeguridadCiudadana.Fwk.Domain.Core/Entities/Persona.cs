//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class Persona : EntityBase
    {
        public int PersonaId { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Genero { get; set; }
        public Nullable<int> TipoDocumento { get; set; }
        public Nullable<int> Documento { get; set; }
        public Nullable<System.DateTime> FechaNacimiento { get; set; }
        public Nullable<System.DateTime> FechaRegistracion { get; set; }
        public string TelefonoCelular { get; set; }
        public string TelefonoOpcional { get; set; }
        public string Email { get; set; }
        public string Calle { get; set; }
        public Nullable<int> CodigoPostal { get; set; }
        public string PathImagen { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> PaisId { get; set; }
        public Nullable<int> ProvinciaId { get; set; }
        public Nullable<int> LocalidadId { get; set; }
        public string UserId { get; set; }
    
        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual Localidad Localidad { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual Provincia Provincia { get; set; }
    }
}
