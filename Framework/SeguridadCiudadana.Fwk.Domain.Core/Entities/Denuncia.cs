//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeguridadCiudadana.Fwk.Domain.Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class Denuncia : EntityBase
    {
        public int DenunciaId { get; set; }
        public string Descripcion { get; set; }
        public Nullable<System.DateTime> FechaHora { get; set; }
        public Nullable<int> TipoPersona { get; set; }
        public string UserId { get; set; }
    
        public virtual AspNetUsers AspNetUsers { get; set; }
    }
}
