﻿using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Logging;
using SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.Core.Mailing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.NetFramework.Mailing
{
    
    public class MailingService:IMailing
    {
        public void SendEmail(string[] to, string[] cc, string[] bcc, string subject, string body, string account)
        {
            MailAddress from = new MailAddress(account, SourceName);
            MailMessage mailmsj = new MailMessage();
            mailmsj.From = from;

            foreach (var item in to)
            {
                mailmsj.To.Add(new MailAddress(item));
            }

            foreach (var item in cc)
            {
                mailmsj.CC.Add(new MailAddress(item));
            }
            
            foreach (var item in bcc)
            {
                mailmsj.Bcc.Add(new MailAddress(item));
            }

            if (!string.IsNullOrEmpty(MailBcc))
            {
                mailmsj.Bcc.Add(new MailAddress(MailBcc));
            }

            mailmsj.Subject = subject;
            mailmsj.Body = body;
            mailmsj.IsBodyHtml = true;

            try
            {
                SmtpClient client = new SmtpClient();
                client.Credentials = new System.Net.NetworkCredential(SourceAddress, SourcePassword);
                client.Host = SourceHost;
                client.Port = ServerPort;
                client.EnableSsl = EnableSsl;
                client.Send(mailmsj);
            }
            catch (Exception ex)
            {
                var inner = ex.InnerException != null ? ex.InnerException.Message : string.Empty;
                LoggerFactory.Instance.LogError("Error Envio Mail Asunto: {0} Detalle: {1} Inner: {2}", subject, ex.Message,inner);
            }
        
        }

        public string SourceAddress
        {
            get;set;
        }

        public string SourceName
        {
            get;
            set;
        }

        public string SourcePassword
        {
            get;
            set;
        }

        public string SourceHost
        {
            get;
            set;
        }

        public string MailBcc { get; set; }
        public int ServerPort { get; set; }
        public bool EnableSsl { get; set; }
    }
}
