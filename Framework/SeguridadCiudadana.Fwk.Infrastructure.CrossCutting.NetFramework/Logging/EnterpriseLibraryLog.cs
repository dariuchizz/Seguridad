﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.NetFramework.Logging
{
    public class EnterpriseLibraryLog:ILogger
    {
        #region Members

        readonly LogWriter _source;

        #endregion

        #region  Constructor

        /// <summary>
        /// Create a new instance of this trace manager
        /// </summary>
        public EnterpriseLibraryLog()
        {
            // Create default source
            var logWriterFactory = new LogWriterFactory();
            _source = logWriterFactory.Create();
        }

        #endregion
        public void Debug(string message, params object[] args)
        {
            if (_source.IsLoggingEnabled())
            {
                var messageToTrace = String.Format(CultureInfo.InvariantCulture, message, args);
                _source.Write(messageToTrace,"Debug",1,0,TraceEventType.Information);
            }
        }

        public void Debug(string message, Exception exception, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Debug(object item)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, Exception exception, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void LogInfo(string message, params object[] args)
        {
            if (_source.IsLoggingEnabled())
            {
                var messageToTrace = String.Format(CultureInfo.InvariantCulture, message, args);
                _source.Write(messageToTrace,"General" ,1 ,0 , TraceEventType.Information);
            }
        }

        public void LogWarning(string message, params object[] args)
        {
            if (_source.IsLoggingEnabled())
            {
                var messageToTrace = String.Format(CultureInfo.InvariantCulture, message, args);
                _source.Write(messageToTrace, "General", 1, 0, TraceEventType.Warning);
            }
        }

        public void LogError(string message, params object[] args)
        {
            if (_source.IsLoggingEnabled())
            {
                var messageToTrace = String.Format(CultureInfo.InvariantCulture, message, args);
                _source.Write(messageToTrace, "General", 1, 0, TraceEventType.Error);
            }
        }

        public void LogError(string message, Exception exception, params object[] args)
        {
            throw new NotImplementedException();
        }
    }
}
