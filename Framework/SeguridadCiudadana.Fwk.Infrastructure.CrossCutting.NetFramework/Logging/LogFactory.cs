﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Logging;

namespace SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.NetFramework.Logging
{
    public class LogFactory<TL> : ILoggerFactory
                where TL : new()
    {
        public ILogger Create()
        {
            return new TL() as ILogger;
        }
    }
}
