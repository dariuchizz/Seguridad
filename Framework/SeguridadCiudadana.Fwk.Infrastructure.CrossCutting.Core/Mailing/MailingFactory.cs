﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.Core.Mailing
{
    /// <summary>
    /// MailingFactory  implementation 
    /// </summary>
    public sealed class MailingFactory
    {
        #region Singleton

        private readonly static Lazy<MailingFactory> _instance = new Lazy<MailingFactory>(() => new MailingFactory());

        /// <summary>
        /// Get singleton instance of IoCFactory
        /// </summary>
        public static MailingFactory Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion

        #region Members

        IMailing _current;

        /// <summary>
        /// Get current configured IContainer
        /// <remarks>
        /// At this moment only IoCUnityContainer existss
        /// </remarks>
        /// </summary>
        public IMailing Current
        {
            get
            {
                if (_current == null)
                    throw new InvalidOperationException("Set the container before using");
                return _current;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Only for singleton pattern, remove before field init IL anotation
        /// </summary>
        static MailingFactory() { }
        MailingFactory()
        {
           
        }

        public void SetMailing(IMailing mailing, NameValueCollection config)
        {
            _current = mailing;
            mailing.SourceAddress = config.GetValues("siteEmail").FirstOrDefault();
            mailing.SourceName = config.GetValues("siteName").FirstOrDefault();
            mailing.SourcePassword = config.GetValues("siteEmailPassword").FirstOrDefault();
            mailing.SourceHost = config.GetValues("smtpHost").FirstOrDefault();
            mailing.MailBcc = config.GetValues("mailBcc").FirstOrDefault();
            mailing.ServerPort = Convert.ToInt32(config.GetValues("serverPort").FirstOrDefault());
            mailing.EnableSsl = Convert.ToBoolean(config.GetValues("enableSsl").FirstOrDefault());
        }

        #endregion

    }
}
