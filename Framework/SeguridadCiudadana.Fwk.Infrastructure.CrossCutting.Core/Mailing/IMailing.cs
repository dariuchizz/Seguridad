﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.Core.Mailing
{
    public interface IMailing
    {
        string SourceAddress { get; set; }
        string SourceName { get; set; }
        string SourcePassword { get; set; }
        string SourceHost { get; set; }
        string MailBcc { get; set; }
        int ServerPort { get; set; }
        bool EnableSsl { get; set; }
        void SendEmail(string[] to, string[] cc, string[] bcc, string subject, string body, string account);
    }
}
