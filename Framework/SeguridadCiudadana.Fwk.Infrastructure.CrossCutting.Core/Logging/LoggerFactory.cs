﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeguridadCiudadana.Fwk.Infrastructure.Crosscutting.Core.Logging
{
    /// <summary>
    /// Log Factory
    /// </summary>
    public static class LoggerFactory
    {
        #region Members

        static ILogger _currentLog = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Set the  log factory to use
        /// </summary>
        /// <param name="logFactory">Log factory to use</param>
        public static void SetCurrent(ILoggerFactory logFactory)
        {
            _currentLog = logFactory.Create();
        }

        /// <summary>
        /// Createt a new <paramref name="SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.Core.Logging.ILogger"/>
        /// </summary>
        /// <returns>Created ILog</returns>
        //public static ILogger CreateLog()
        //{
        //    return (_currentLog != null) ? _currentLog.Create() : null;
        //}

        public static ILogger Instance { get { return _currentLog; } }

        #endregion
    }
}
