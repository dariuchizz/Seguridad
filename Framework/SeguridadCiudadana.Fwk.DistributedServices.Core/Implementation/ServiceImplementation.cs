﻿using SeguridadCiudadana.Fwk.DistributedServices.Core.ErrorHandlers;
using SeguridadCiudadana.Fwk.DistributedServices.Core.InstanceProviders;
using SeguridadCiudadana.Fwk.Infrastructure.CrossCutting.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace SeguridadCiudadana.Fwk.DistributedServices.Core.Implementation
{
    [ApplicationErrorHandlerAttribute()]
    //[IoCInstanceProviderServiceBehavior()]
    public class ServiceImplementation<L>
        where L:class
    {
        #region Constructor
        public ServiceImplementation() { }
        #endregion

        [Dependency]
        public L BusinessLogic { get; set; }
    }
}
